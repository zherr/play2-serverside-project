/**
 * 
 */
package courseTests;

import models.course.CourseCatalog;
import models.course.Section;
import models.utility.TimeStamp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author zherr
 *
 */
public class TestCourseCatalog {
	
	private CourseCatalog cat0;
	private CourseCatalog cat1;
    private CourseCatalog cat2;
	private Section c0, c1, c2;
    private TimeStamp timeStamp0;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		cat0 = new CourseCatalog();
		cat1 = new CourseCatalog();
		c0 = new Section();
		c1 = new Section();
		c2 = new Section();
        timeStamp0 = new TimeStamp();
		List<Section> courseList = new ArrayList<Section>();
		courseList.add(c0);
		courseList.add(c1);
		courseList.add(c2);		
		cat0.setDateUpdated(timeStamp0);
		cat0.setAllSections(courseList);
//        cat2 = new CourseCatalog(timeStamp0, new ArrayList<Section>());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertTrue(cat1.getAllSections().size() == 0);
		assertNull(cat1.getDateUpdated());
		assertNotNull(cat0.getDateUpdated());
		assertTrue(cat0.getAllSections().size() == 3);
	}

}
