/**
 * 
 */
package courseTests;

import models.course.Section;
import models.users.Professor;
import models.users.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author zherr
 *
 */
public class TestSection {
	
	private Section cOffer0;
	private Section cOffer1;
    private Section cOffer2;
	private Professor p0;
    private Student s0, s1;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		cOffer0 = new Section();
		cOffer1 = new Section();
		p0 = new Professor();
        s0 = new Student();
        s1 = new Student();
        List<Student> allStudents = new ArrayList<Student>();
        allStudents.add(s0);
        allStudents.add(s1);
		
		cOffer1.setCourseName("Astrology 101");
        cOffer1.setSectionId(1);
        cOffer1.setCourseId(1);
        cOffer1.setProfessorId(1);
		cOffer1.setDescription("A basic class about the stars");
		cOffer1.setSectionNumber("001");
		cOffer1.setSemester("Fall");
        cOffer1.setYearOffered("2014");
		cOffer1.setStudents(allStudents);
        cOffer2 = new Section(0, 0 ,0, "234", "fall", "8998", allStudents);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNull(cOffer0.getCourseName());
		assertNull(cOffer0.getDescription());
		assertNull(cOffer0.getSectionNumber());
		assertNull(cOffer0.getSemester());

		assertTrue(cOffer1.getCourseName().equals("Astrology 101"));
		assertTrue(cOffer1.getDescription().length() > 0);
        assertTrue(cOffer1.getSectionId() == 1);
        assertTrue(cOffer1.getCourseId() == 1);
        assertTrue(cOffer1.getProfessorId() == 1);
		assertTrue(cOffer1.getSectionNumber().equals("001"));
		assertTrue(cOffer1.getSemester().equals("Fall"));
		assertNotNull(cOffer1.getStudentRoster());
        assertNotNull(cOffer0.getStudentRoster());
        assertTrue(cOffer1.getStudentRoster().size() == 2);
	}

}
