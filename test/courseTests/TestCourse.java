/**
 * 
 */
package courseTests;

import models.billing.Cash;
import models.course.Course;
import models.course.Section;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author zherr
 *
 */
public class TestCourse {
	
	private Course testCourse0;
	private Course testCourse1;
	private Course testCourse2;
    private Course testCourse3;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		testCourse0 = new Section();
		testCourse1 = new Section();
		testCourse2 = new Section();
        testCourse1.setCourseId(1);
        testCourse1.setPrerequisiteId(0);
		testCourse1.setCourseName("Biology 201");
		testCourse1.setDescription("This class is a continuation of Biology 101");
		testCourse2.setCourseName("Biology 101");
		final ArrayList<Course> preReq = new ArrayList<Course>();
		preReq.add(testCourse2);
		testCourse1.setCoursePrerequisites(preReq);
        Cash cost0 = new Cash();
        cost0.setAmount(375);
        testCourse1.setCostOfClass(cost0);
        testCourse3 = new Course(0, 0, "test", "test", new ArrayList<Course>(), cost0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNull(testCourse0.getCourseName());
		assertNull(testCourse0.getDescription());
		assertTrue(testCourse0.getPrerequisites().size() == 0);
		assertTrue(testCourse1.getCourseName().equals("Biology 201"));
		assertTrue(testCourse1.getDescription().length() > 0);
		assertTrue(testCourse1.getPrerequisites().size() == 1);
        assertTrue(testCourse1.getCourseId() == 1);
        assertTrue(testCourse1.getPrerequisiteId() == 0);
		assertTrue(testCourse2.getCourseName().equals("Biology 101"));
		assertTrue(testCourse2.getPrerequisites().size() == 0);
		
		assertTrue(testCourse1.getPrerequisites().get(0) == testCourse2);
        assertTrue(testCourse1.getCostOfClass().getAmount() == 375);
	}

}
