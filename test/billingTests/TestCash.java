/**
 * 
 */
package billingTests;

import models.billing.Cash;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author zherr
 *
 */
public class TestCash {
	
	private Cash cash0, cash1, cash2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		cash0 = new Cash();
		cash1 = new Cash();
		cash0.setAmount(10.54);
        cash2 = new Cash(11.00);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertTrue(cash1.getAmount() == 0);
		assertTrue(cash0.getAmount() == 10.54);

        cash0.addAmount(5);
        assertTrue(cash0.getAmount() == 15.54);

        cash0.subtractAmount(3);
        assertTrue(cash0.getAmount() == 12.54);
	}

}
