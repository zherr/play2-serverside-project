/**
 * 
 */
package billingTests;

import models.billing.BillReport;
import models.billing.Cash;
import models.utility.TimeStamp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author zherr
 *
 */
public class TestBillReport {
	
	private BillReport br0, br1, br3;
    private Cash cash0, cash1;
    private TimeStamp timeStamp;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		br0 = new BillReport();
		br1 = new BillReport();
//        br3 = new BillReport(3, cash0, timeStamp, 0);
        cash0 = new Cash();
        cash1 = new Cash();
        timeStamp = new TimeStamp();
        cash0.setAmount(5.00);
        cash1.setAmount(10.00);
		br0.setTotalBalance(cash1);
//        br0.setTimeStamp(timeStamp);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNull(br1.getTotalBalance());

		assertNotNull(br0.getTotalBalance());

        assertTrue(br0.getTotalBalance().getAmount() == 10);

        br0.adjustBalance(cash0);
        assertTrue(br0.getTotalBalance().getAmount() == 15);

//        assertNotNull(br0.getTimeStamp());
	}

}
