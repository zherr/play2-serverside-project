/**
 *
 */
package billingTests;

import models.billing.BillReport;
import models.billing.Bursar;
import models.billing.Cash;
import models.course.Section;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import viewmodels.ViewSection;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author zherr
 *
 */
public class TestBursar {

    private Bursar bursar;
    private BillReport billWeOwe;
    private Cash whatWePay;
    private ArrayList<ViewSection> ourClasses = new ArrayList<ViewSection>();

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        bursar = new Bursar();
        billWeOwe = new BillReport();
        Section c0, c1;
        c0 = new Section();
        c1 = new Section();
        Cash cash0 = new Cash();
        Cash cash1 = new Cash();
        whatWePay = new Cash();
        whatWePay.setAmount(700);
        cash0.setAmount(300.00);
        cash1.setAmount(450.00);
        c0.setCostOfClass(cash0);
        c1.setCostOfClass(cash1);
        ourClasses.add(new ViewSection(c0));
        ourClasses.add(new ViewSection(c1));
        Cash weOwe = new Cash();
        weOwe.setAmount(cash0.getAmount() + cash1.getAmount());
        billWeOwe.setTotalBalance(weOwe);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
//        billWeOwe = bursar.GenerateEBill(ourClasses);
//        assertTrue(billWeOwe.getTotalBalance().getAmount() == 750);

        boolean paidInFull = bursar.PayBill(billWeOwe, whatWePay);
        assertFalse(paidInFull);
        System.out.println("total Balance: " + billWeOwe.getTotalBalance().getAmount());
        assertTrue(billWeOwe.getTotalBalance().getAmount() == 50);
    }

}
