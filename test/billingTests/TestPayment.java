/**
 * 
 */
package billingTests;

import models.billing.Cash;
import models.billing.Payment;
import models.utility.Time;
import models.utility.TimeStamp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @author zherr
 *
 */
public class TestPayment {
	
	private Payment pay0, pay1, pay2;
    private TimeStamp t0;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		pay0 = new Payment();
		pay1 = new Payment();
        t0 = new TimeStamp();
        Time time0 = new Time();
        t0.setTime(time0);
		pay0.setPayment(new Cash());		
		pay0.setTimeStamp(t0);
        pay2 = new Payment(new Cash(), t0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNull(pay1.getPayment());
		assertNull(pay1.getTimeStamp());
		
		assertNotNull(pay0.getPayment());
        assertNotNull(pay0.getTimeStamp());
		assertNotNull(pay0.getTimeStamp().getTime().toString().equals("00:00:00:00 AM"));
	}

}
