package utilityTests;

import models.utility.Time;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 9/30/13
 * Time: 10:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class TestTime {

    private Time time0, time1;

    @Before
    public void setUp() throws Exception {
        time0 = new Time();
        time1 = new Time();
        time0.setHours(5);
        time0.setMinutes(45);
        time0.setSeconds(0);
        time0.setmSecs(0);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void test() throws Exception {
        assertTrue(time0.getHours() == 5);
        assertTrue(time0.getMinutes() == 45);
        assertTrue(time0.getSeconds() == 0);
        assertTrue(time0.getmSecs() == 0);
        assertTrue(time0.toString().equals("05:45:00"));

        assertTrue(time1.getHours() == 0);
        assertTrue(time1.getMinutes() == 0);
        assertTrue(time1.getSeconds() == 0);
        assertTrue(time1.getmSecs() == 0);
        assertTrue(time1.toString().equals("00:00:00"));
    }
}
