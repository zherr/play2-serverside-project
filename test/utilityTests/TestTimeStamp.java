package utilityTests;

import models.utility.Time;
import models.utility.TimeStamp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 9/30/13
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class TestTimeStamp {

    private TimeStamp ts0, ts1, ts2;
    private Time t0;

    @Before
    public void setUp() throws Exception {
        ts0 = new TimeStamp();
        ts1 = new TimeStamp();
        t0 = new Time();

        t0.setHours(5);
        t0.setMinutes(55);

        ts0.setDay(15);
        ts0.setMonth(9);
        ts0.setTime(new Time());
        ts0.setYear(1991);
        ts0.setTime(t0);

        ts2 = new TimeStamp("2013-09-14 03:33:00");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void test()throws Exception{
        assertTrue(ts0.getDay() == 15);
        assertTrue(ts0.getMonth() == 9);
        assertTrue(ts0.getYear() == 1991);
        assertTrue((ts0.getTime().toString().equals("05:55:00")));

        assertNull(ts1.getTime());

        assertTrue(ts2.getDay() == 14);
        assertTrue(ts2.getMonth() == 9);
        assertTrue(ts2.getYear() == 2013);
        assertTrue(ts2.getTime().getHours() == 3);
        assertTrue(ts2.getTime().getMinutes() == 33);
        assertTrue(ts2.getTime().getSeconds() == 00);
        assertTrue(ts2.getTime().getmSecs() == 0);

        assertTrue(ts2.toString().equals("2013-09-14 03:33:00"));
    }
}
