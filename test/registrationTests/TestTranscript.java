/**
 * 
 */
package registrationTests;

import models.registration.Grade;
import models.registration.Transcript;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * @author zherr
 *
 */
public class TestTranscript {
	
	private Transcript tran0, tran1, tran2;
	private Grade g0, g1, g2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		tran0 = new Transcript();
		tran1 = new Transcript();
		g0 = new Grade();
		g1 = new Grade();
		g2 = new Grade();
		g0.setGradeLetter('A');
		g1.setGradeLetter('B');
		g2.setGradeLetter('C');
		HashMap<String, Grade> gradeMap = new HashMap<String, Grade>();
		gradeMap.put("Biology", g0);
		gradeMap.put("Data Structures", g1);
		gradeMap.put("Astrology", g2);
		tran0.setGradeMap(gradeMap);
        tran0.setTranscriptId(1);
        tran0.setGrade(g0);
        tran0.setSectionId(1);
        tran0.setStudentId(1);
        tran2 = new Transcript(gradeMap, 0, g0, 0, 0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNull(tran1.getGradeForCourseName("test"));
		assertNotNull(tran1.getGradeMap());
		
		assertTrue(tran0.getGradeMap().size() == 3);
		assertTrue(tran0.getGradeForCourseName("Biology").getGradeLetter() == 'A');
		assertTrue(tran0.getGradeForCourseName("Data Structures").getGradeLetter() == 'B');
		assertTrue(tran0.getGradeForCourseName("Astrology").getGradeLetter() == 'C');
        assertTrue(tran0.getGrade() == g0);
        assertTrue(tran0.getTranscriptId() == 1);
        assertTrue(tran0.getSectionId() == 1);
        assertTrue(tran0.getStudentId() == 1);
	}

}
