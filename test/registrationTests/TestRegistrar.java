/**
 * 
 */
package registrationTests;

import models.course.Course;
import models.course.Section;
import models.registration.Registrar;
import models.users.Professor;
import models.users.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * @author zherr
 *
 */
public class TestRegistrar {
	
	private Registrar reg0, reg1, reg2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		reg0 = new Registrar();
		reg1 = new Registrar();
		
		ArrayList<Student> allStudents = new ArrayList<Student>();
		ArrayList<Professor> allProfessors = new ArrayList<Professor>();
		Student s0, s1, s2;
		s0 = new Student();
		s1 = new Student();
		s2 = new Student();
		Professor p0, p1, p2;
		p0 = new Professor();
		p1 = new Professor();
		p2 = new Professor();
		allStudents.add(s0);
		allStudents.add(s1);
		allStudents.add(s2);
		allProfessors.add(p0);
		allProfessors.add(p1);
		allProfessors.add(p2);
				
		reg0.setAllProfessors(allProfessors);
        reg2 = new Registrar(allProfessors, new ArrayList<Course>(), new ArrayList<Section>());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertTrue(reg1.getAllProfessors().size() == 0);

		assertTrue(reg0.getAllProfessors().size() == 3);
	}

}
