/**
 *
 */
package registrationTests;

import models.course.CourseCatalog;
import models.course.Section;
import models.registration.ProfessorRegistration;
import models.registration.Registrar;
import models.users.Professor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zherr
 *
 */
public class TestProfessorRegistration {

    private ProfessorRegistration pReg0;
    private Professor prof0;
    private Professor prof1;
    private Section course0;
    private Section course1;
    private Registrar registrar;
    private CourseCatalog catalog;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        pReg0 = new ProfessorRegistration();
        prof0 = new Professor();
        prof1 = new Professor();
        course0 = new Section();
        course1 = new Section();
        pReg0.RegisterProfessor(prof0, course0);

        List<Professor> allProfs = new ArrayList<Professor>();
        allProfs.add(prof0);
        allProfs.add(prof1);
        List<Section> allCourses = new ArrayList<Section>();
        allCourses.add(course0);
        allCourses.add(course1);
        catalog = new CourseCatalog();
        catalog.setAllSections(allCourses);
        registrar = new Registrar();
        registrar.setAllProfessors(allProfs);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
        // TODO not needing to run tests due to change in functionality
    }

}
