/**
 * 
 */
package registrationTests;

import models.registration.Grade;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author zherr
 *
 */
public class TestGrade {
	
	private Grade g0, g1, g2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		g0 = new Grade();
		g1 = new Grade();
		g0.setGradeLetter('A');
		g0.setGradeNumber(4.0);
        g2 = new Grade(3.00, 'B');
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertTrue(g1.getGradeLetter() == 0);
		assertTrue(g1.getGradeNumber() == 0);
		
		assertTrue(g0.getGradeLetter() == 'A');
		assertTrue(g0.getGradeNumber() == 4.0);
	}

}
