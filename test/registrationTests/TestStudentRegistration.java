/**
 *
 */
package registrationTests;

import models.course.Section;
import models.registration.StudentRegistration;
import models.users.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author zherr
 *
 */
public class TestStudentRegistration {

    private StudentRegistration studentReg;

    private Section c0;
    private Student s0, s1;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        studentReg = new StudentRegistration();
        s0 = new Student();
        c0 = new Section();
        studentReg.RegisterToEnroll(s0, c0);
        studentReg.RegisterToEnroll(s1, c0);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
        assertTrue(c0.getStudentRoster().size() == 2);
    }

}
