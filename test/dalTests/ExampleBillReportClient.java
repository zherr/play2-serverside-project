package dalTests;

import dal.BillReportDAO;
import models.billing.BillReport;
import models.billing.Cash;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/9/13
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleBillReportClient {

    public static void main(String args[]){

        System.out.println(":::::::::: Attempting to create BillReportDAO! ::::::::::");
        BillReportDAO billReportDAO = new BillReportDAO();

        System.out.println(":::::::::: Trying to search for Course in database ::::::::::");
        BillReport searchedBillReport = billReportDAO.getBillReport(1);

        System.out.println("ExampleBillReportClient: *************** Here is searched BillReport information *************************");
        System.out.println("\n\nbillReportId: " + searchedBillReport.getBillReportId());
        System.out.println("total: " + searchedBillReport.getTotalBalance().getAmount());
        System.out.println("dateBilled: " + searchedBillReport.getTimeStamp().toString());
        System.out.println("studentSsn: " + searchedBillReport.getStudentId());
        System.out.println("sectionId: " + searchedBillReport.getCourseId());
        System.out.println("paid: " + searchedBillReport.isPaid());

        //
        System.out.println(":::::::::: Attempting to add a BillReport ::::::::::");
        BillReport addBillReport = new BillReport();
        addBillReport.setBillReportId(2);
        addBillReport.setStudentId(456789876);
        Cash cash = new Cash();
        cash.setAmount(50.00);
        addBillReport.setTotalBalance(cash);
        addBillReport.setCourseId(2);
        addBillReport.setPaid(false);
        billReportDAO.addBillReport(addBillReport);
    }
}
