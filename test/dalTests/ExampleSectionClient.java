package dalTests;

import dal.SectionDAO;
import models.course.Section;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/9/13
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleSectionClient {

    public static void main(String args[]){

        System.out.println(":::::::::: Attempting to create SectionDAO! ::::::::::");
        SectionDAO sectionDAO = new SectionDAO();

        System.out.println(":::::::::: Trying to search for Section in database ::::::::::");
        Section searchedSection = sectionDAO.getSection(1);

        System.out.println("ExampleSectionClient: *************** Here is searched section information *************************");
        System.out.println("\n\nsectionId : " + searchedSection.getSectionId());
        System.out.println("sectionNumber: " + searchedSection.getSectionNumber());
        System.out.println("semesterOffered: " + searchedSection.getSemester());
        System.out.println("yearOffered: " + searchedSection.getYearOffered());
        System.out.println("InstructorId: " + searchedSection.getProfessorId());
        System.out.println("ParentCourseId: " + searchedSection.getCourseId());

        //
        System.out.println(":::::::::: Attempting to add a Section to the database");
        Section addSection = new Section();
//        addSection.setSectionId(7);
        addSection.setSectionNumber("001");
        addSection.setSemester("Fall");
        addSection.setYearOffered("2014");
        addSection.setProfessorId(877777777);
        addSection.setCourseId(1003);
        addSection.setMaxSeats(45);
        addSection.setCurrentSeats(5);
        sectionDAO.addSection(addSection);
    }
}
