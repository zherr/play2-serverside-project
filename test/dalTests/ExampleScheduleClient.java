package dalTests;

import dal.ScheduleDAO;
import viewmodels.ViewSection;
import viewmodels.ViewStudent;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/12/13
 * Time: 11:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleScheduleClient {

    public static void main(String args[]){
        ScheduleDAO schedDao = new ScheduleDAO();

        System.out.print(":::::::::: Attempting to get students by sectionIs ::::::::::");
        ArrayList<ViewStudent> studs = schedDao.getStudentsBySectionId(2);
        for(ViewStudent stud : studs){
            System.out.println(stud.getName());
            System.out.println(stud.getSsn());
        }

        System.out.println(":::::::::: Attempting to get sections by student ssn ::::::::::");
        ArrayList<ViewSection> secs = schedDao.getSectionsByStudentId(456789876);
        for(ViewSection sec : secs){
            System.out.println(sec.getCourseId());
            System.out.println(sec.getSectionNumber());
            System.out.println(sec.getProfessorId());
        }
    }
}
