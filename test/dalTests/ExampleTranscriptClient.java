package dalTests;

import dal.TranscriptDAO;
import models.registration.Grade;
import models.registration.Transcript;
import models.users.Student;
import viewmodels.ViewStudent;
import viewmodels.ViewTranscript;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/9/13
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleTranscriptClient {

    public static void main(String args[]){

        System.out.println(":::::::::: Attempting to create TranscriptDAO! ::::::::::");
        TranscriptDAO transcriptDAO = new TranscriptDAO();

        System.out.println(":::::::::: Trying to search for transcript in database ::::::::::");
        Transcript searchedTranscript = transcriptDAO.getTranscript(1);

        System.out.println("ExampleTranscriptClient: *************** Here is searched transcript information *************************");
        System.out.println("\n\ntranscriptId: " + searchedTranscript.getTranscriptId());
        System.out.println("gradeNumber: " + searchedTranscript.getGrade().getGradeNumber());
        System.out.println("gradeLetter: " + searchedTranscript.getGrade().getGradeLetter());
        System.out.println("studentSsn: " + searchedTranscript.getStudentId());
        System.out.println("sectionId: " + searchedTranscript.getSectionId());

        //
        System.out.println(":::::::::: Attempting to add Transcript to database");
        Transcript addTranscript = new Transcript();
        addTranscript.setSectionId(2);
        addTranscript.setStudentId(456789876);
        addTranscript.setTranscriptId(3);
        Grade grade = new Grade();
        grade.setGradeLetter('A');
        grade.setGradeNumber(4.00);
        addTranscript.setGrade(grade);
        transcriptDAO.addTranscript(addTranscript);

        //
        System.out.println(":::::::::: Attempting to get transcripts by studentview ssn from database");
        Student testStudent = new Student("Student", "compsci");
        testStudent.setSsn(456789876);
        ViewStudent vStudent = new ViewStudent(testStudent);
        ArrayList<ViewTranscript> transcripts = transcriptDAO.getTranscriptsStringByStudent(vStudent);
        for(ViewTranscript tran : transcripts){
            System.out.println(tran.getCourseName());
            System.out.println(tran.getGradeLetter());
        }

    }
}
