package dalTests;

import models.registration.StudentRegistration;
import models.users.Student;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/4/13
 * Time: 6:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleStudentClient {
    public static void main( String args[] ) throws Exception{

        System.out.println(" ********** Creating Student Registration Object **********");
        StudentRegistration studRegistration = new StudentRegistration();

        System.out.println(" ********** Trying to search for studentview in the database **********");

        Student searchedStudent = studRegistration.findStudentById(456789876);

        System.out.println("ExampleStudentClient: *************** Here is searched studentview information *************************");
        System.out.println("\tName: \t\t\t" + searchedStudent.getName() + "\n");
        int ssn = searchedStudent.getSsn();
        String phone = searchedStudent.getPhone();
        String email = searchedStudent.getEmail();
        String address = searchedStudent.getAddress();
        String city = searchedStudent.getCity();
        String state = searchedStudent.getState();
        int zip = searchedStudent.getZip();
        String major = searchedStudent.getMajor();
        String classStatus = searchedStudent.getClassStatus();

        System.out.println("Ssn: " + ssn);
        System.out.println("Phone: " + phone);
        System.out.println("Email: " + email);
        System.out.println("Location: " + address + ", " + city + ", " + state + ", " + zip);
        System.out.println("Major: " + major);
        System.out.println("Status: " + classStatus);

        Student addStudent = searchedStudent;
        addStudent.setSsn(876898765);
        addStudent.setName("Katherine Welz");
        addStudent.setMajor("Psychology");
        System.out.println(":::::::::: Adding new studentview to database! ::::::::::");
        studRegistration.addStudent(addStudent);
    }
}
