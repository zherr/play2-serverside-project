package dalTests;

import models.registration.ProfessorRegistration;
import models.users.Professor;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/6/13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleProfessorClient {

    public static void main(String args[]) throws Exception {

        System.out.println(" ********** Creating Professor Registration Object **********");
        ProfessorRegistration profRegistration = new ProfessorRegistration();

        System.out.println(" ********** Trying to search for professor in the database **********");

        Professor searchedProfessor = profRegistration.findProfessorById(877777777);

        System.out.println("ExampleProfessorClient: *************** Here is searched professor information *************************");
        System.out.println("\tName: \t\t\t" + searchedProfessor.getName() + "\n");
        int ssn = searchedProfessor.getSsn();
        String phone = searchedProfessor.getPhone();
        String email = searchedProfessor.getEmail();
        String address = searchedProfessor.getAddress();
        String city = searchedProfessor.getCity();
        String state = searchedProfessor.getState();
        int zip = searchedProfessor.getZip();
        String title = searchedProfessor.getTitle();
        int salary = searchedProfessor.getSalary();

        System.out.println("Ssn: " + ssn);
        System.out.println("Phone: " + phone);
        System.out.println("Email: " + email);
        System.out.println("Location: " + address + ", " + city + ", " + state + ", " + zip);
        System.out.println("Title: " + title);
        System.out.println("Salary: " + salary);

        Professor addProfessor = searchedProfessor;
        addProfessor.setSsn(878888888);
        addProfessor.setName("Konstantin Laufer");
        addProfessor.setTitle("Dr.");
        addProfessor.setSalary(100000);
        System.out.println(":::::::::: Adding new professor to database! ::::::::::");
        profRegistration.addProfessor(addProfessor);
    }
}
