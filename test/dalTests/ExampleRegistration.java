package dalTests;

import dal.RegistrarDAO;
import viewmodels.ViewCourse;
import viewmodels.ViewProfessor;
import viewmodels.ViewSection;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/16/13
 * Time: 10:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleRegistration {

    public static void main(String args[]){

        RegistrarDAO dao = new RegistrarDAO();

        ArrayList<ViewSection> sections = dao.getAllSections();
        for(ViewSection sec : sections){
            System.out.println(sec.getCourseId());
            System.out.println(sec.getSectionNumber());
            System.out.println(sec.getProfessorId());
        }
        ArrayList<ViewCourse> courses = dao.getCoursesBySections(sections);
        for(ViewCourse course : courses){
            System.out.println(course.getCourseName());
            System.out.println(course.getDescription());
            System.out.println(course.getCostOfClass());
        }
        ArrayList<ViewProfessor> profs = dao.getProfessorsBySections(sections);
        for(ViewProfessor prof : profs){
            System.out.println(prof.getName());
            System.out.println(prof.getAddress());
            System.out.println(prof.getEmail());
        }

        System.out.println("Size of sections: " + sections.size());
        System.out.println("Size of courses: " + courses.size());
        System.out.println("Size of profs: " + profs.size());
    }
}
