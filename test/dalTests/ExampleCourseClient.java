package dalTests;

import dal.CourseDAO;
import models.billing.Cash;
import models.course.Course;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/8/13
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExampleCourseClient {

    public static void main(String args[]){

        System.out.println(":::::::::: Attempting to create CourseDAO! ::::::::::");
        CourseDAO courseDAO = new CourseDAO();

        System.out.println(":::::::::: Trying to search for Course in database ::::::::::");
        Course searchedCourse = courseDAO.getCourse(1);

        System.out.println("ExampleCourseClient: *************** Here is searched course information *************************");
        System.out.println("\n\ncourseId: " + searchedCourse.getCourseId());
        System.out.println("prereqId: " + searchedCourse.getPrerequisiteId());
        System.out.println("Name: " + searchedCourse.getCourseName());
        System.out.println("Descr: " + searchedCourse.getDescription());
        System.out.println("Cost: " + searchedCourse.getDescription());

        System.out.println(":::::::::: Trying to search for course prerequisites! ::::::::::");
        List<Course> prerequisites = new ArrayList<Course>();
        prerequisites = courseDAO.getCoursePrerequisites(1);
        System.out.println("ExampleCourseClient: *************** Here is searched course PREREQUISITE information *************************");
        for( Course c : prerequisites ){
            System.out.println("\n\ncourseId: " + c.getCourseId());
            System.out.println("prereqId: " + c.getPrerequisiteId());
            System.out.println("Name: " + c.getCourseName());
            System.out.println("Descr: " + c.getDescription());
            System.out.println("Cost: " + c.getDescription());
        }
        if( prerequisites.size() == 0 ){
            System.out.println("!!!!!!!!!! Did not find any prerequisites !!!!!!!!!!");
        }

        //
        System.out.println(":::::::::: Attempting to add a new Course to the database ::::::::::");
        Course addCourse = new Course();
        addCourse.setCourseId(4);
        addCourse.setPrerequisiteId(0);
        addCourse.setCourseName("Data Structures");
        addCourse.setDescription("This course covers the fundamental data types of modern computers.");
        Cash cost = new Cash();
        cost.setAmount(350.00);
        addCourse.setCostOfClass(cost);
        courseDAO.addCourse(addCourse);
    }
}
