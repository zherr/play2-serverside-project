/**
 * 
 */
package userTests;

import models.users.Professor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author zherr
 *
 */
public class TestProfessor {
	
	private Professor p0, p1, p2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		p0 = new Professor();
		p1 = new Professor();
		p0.setName("Bob Barker");
		p0.setTitle("Doctor");
        p2 = new Professor("Doctor", 10000);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNull(p1.getName());
		assertNull(p1.getTitle());
		
		assertTrue(p0.getName().equals("Bob Barker"));
		assertTrue(p0.getTitle().equals("Doctor"));
	}

}
