/**
 * 
 */
package userTests;

import models.users.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author zherr
 *
 */
public class TestStudent {
	
	private Student s0, s1, s2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		s0 = new Student();
		s1 = new Student();
		s0.setName("Zachary Herr");
		s0.setClassStatus("Senior");
		s0.setMajor("Computer Science");
		s0.setPhone("999-999-9999");
		s0.setEmail("zherr@luc.edu");
        s0.setAddress("1234 Loyola Drive");
        s0.setCity("Chicago");
        s0.setState("IL");
        s0.setZip(12345);
        s2 = new Student("Junior", "Computer Science");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertNull(s1.getEmail());
		assertNull(s1.getMajor());
		assertNull(s1.getPhone());
		assertNull(s1.getName());
		assertNull(s1.getClassStatus());
		
		assertTrue(s0.getName().equals("Zachary Herr"));
		assertTrue(s0.getClassStatus().equals("Senior"));
		assertTrue(s0.getMajor().equals("Computer Science"));
		assertTrue(s0.getPhone().equals("999-999-9999"));
		assertTrue(s0.getEmail().equals("zherr@luc.edu"));
        assertTrue(s0.getAddress().equals("1234 Loyola Drive"));
        assertTrue(s0.getCity().equals("Chicago"));
        assertTrue(s0.getState().equals("IL"));
        assertTrue(s0.getZip() == 12345);
	}

}
