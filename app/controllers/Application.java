package controllers;

import dal.UserDAO;
import models.registration.StudentRegistration;
import models.users.Student;
import models.users.User;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import viewmodels.ViewStudent;
import views.html.login;
import views.html.output;

import static play.data.Form.form;

public class Application extends Controller {


    // TODO better authentication
    public static class Login{
        @Constraints.Required
        public int ssn;

        public String validate(){
            //TODO this could be professor as well... we are just checking if we have someone with this ssn in the database
            StudentRegistration reg = new StudentRegistration();
            ViewStudent vStudent = new ViewStudent(reg.findStudentById(ssn));
            if (0 == vStudent.getSsn()){
                return "Sorry, you are not registered with the university.";
            } else{
                return null;
            }
        }
    }

    public static Result login() {
        return ok(
                login.render(form(Login.class))
        );
    }

    public static Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect(
                routes.Application.login()
        );
    }

    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        } else {
            session().clear();
            session("ssn", Integer.toString(loginForm.get().ssn));
            return resolveUserType(Integer.toString(loginForm.get().ssn));
        }
    }

    @Security.Authenticated(Secured.class)
    public static Result index() {
//        return ok(views.html.index.render("Your new application is ready."));
        return resolveUserType(request().username());
    }
    
    public static Result outputExample() {
        StudentRegistration reg = new StudentRegistration();
        Student searchedStudent = reg.findStudentById(876898765);
        String testString = searchedStudent.getName();
        testString += "\n" + searchedStudent.getMajor();
        testString += "\n" + searchedStudent.getClassStatus();
        testString += "\n" + searchedStudent.getAddress();
        testString += "\n" + searchedStudent.getCity();
        testString += "\n" + searchedStudent.getState();
    	return ok(
    			output.render(testString));
    }

    private static Result resolveUserType(String ssn){
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUser(ssn);
        // it's a studentview
        if(user.getSalary() == 0){
            return redirect(controllers.studentcontrollers.routes.StudentController.index(user.getSsn()));
        }
        // it's a professor
        else{
            return redirect(controllers.professorcontrollers.routes.ProfessorController.index(user.getSsn()));
        }
    }
}
