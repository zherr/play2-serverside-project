package controllers.studentcontrollers;

import dal.BillReportDAO;
import dal.ScheduleDAO;
import models.billing.Bursar;
import play.mvc.Controller;
import play.mvc.Result;
import viewmodels.ViewBillReport;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/17/13
 * Time: 7:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class EBillController extends Controller {

    public static Result index( int ssn ){

        BillReportDAO billDAO = new BillReportDAO();
        ArrayList<ViewBillReport> billReports = billDAO.getBillReportsByStudentSsn(ssn);
        return ok(views.html.studentview.ebill.render(billReports, ssn));
    }

    public static Result payBill( int ssn, int billReportId ){

        Bursar bursar = new Bursar();
        boolean result = bursar.PayBill(billReportId);
        if(result){
            BillReportDAO billDAO = new BillReportDAO();
            ArrayList<ViewBillReport> billReports = billDAO.getBillReportsByStudentSsn(ssn);
            return ok();
        }
        else{
            return badRequest();
        }
    }
}
