package controllers.studentcontrollers;

import dal.TranscriptDAO;
import models.registration.StudentRegistration;
import play.mvc.Controller;
import play.mvc.Result;
import viewmodels.ViewStudent;
import viewmodels.ViewTranscript;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/10/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class TranscriptsController extends Controller {

    public static Result index(int ssn){

        StudentRegistration reg = new StudentRegistration();
        ViewStudent vStudent = new ViewStudent(reg.findStudentById(ssn));
        TranscriptDAO transcriptDAO = new TranscriptDAO();
        List<ViewTranscript> transcripts = transcriptDAO.getTranscriptsStringByStudent(vStudent);
        return ok(views.html.studentview.transcripts.render(ssn, transcripts));
    }
}
