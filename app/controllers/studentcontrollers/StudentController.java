package controllers.studentcontrollers;

import controllers.Secured;
import dal.UserDAO;
import models.registration.StudentRegistration;
import models.users.User;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import viewmodels.ViewStudent;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/7/13
 * Time: 11:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class StudentController extends Controller {

    private static ViewStudent student;

    @Security.Authenticated(Secured.class)
    public static Result index(int ssn){
        //TODO again, this code is smelly. Technically we shouldn't have to check this.
        if(!isStudent(ssn)){
            return redirect(controllers.professorcontrollers.routes.ProfessorController.index(ssn));
        }
        StudentRegistration reg = new StudentRegistration();
        ViewStudent vStudent = new ViewStudent(reg.findStudentById(ssn));
        student = vStudent;
        return ok(views.html.studentview.student.render(vStudent));
    }

    public static Result transcripts(int ssn){
        return redirect("/transcripts");
    }

    private static boolean isStudent(int ssn){
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUser(Integer.toString(ssn));
        // it's a studentview
        if(user.getSalary() == 0){
            return true;
        }
        // it's a professor
        else{
            return false;
        }
    }
}
