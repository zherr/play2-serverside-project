package controllers.studentcontrollers;

import dal.RegistrarDAO;
import dal.StudentDAO;
import play.mvc.Controller;
import play.mvc.Result;
import viewmodels.ViewStudent;

/**
 * Created with IntelliJ IDEA.
 * User: Joe
 * Date: 11/17/13
 * Time: 1:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyStudentInformationController extends Controller{

    public static Result index(int ssn){
        StudentDAO studDao = new StudentDAO();
        ViewStudent student = new ViewStudent(studDao.getStudent(ssn));
        return ok(views.html.studentview.mystudentinformation.render(student, ssn));
    }

    public static Result editField(int ssn, String field, String value){
        //Just in case the user gets sneaky...
        if(field.toLowerCase().equals("ssn") || field.toLowerCase().equals("name") ||
                field.toLowerCase().equals("classstatus")){
            return badRequest();
        }
        System.out.println("Field: " + field);
        System.out.println("Value: " + value);
        RegistrarDAO registrarDAO = new RegistrarDAO();
        boolean result = registrarDAO.updateUserInformation(ssn, field, value);
        if(result){
            return ok();
        }
        else{
            return badRequest();
        }
    }
}
