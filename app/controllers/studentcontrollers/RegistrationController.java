package controllers.studentcontrollers;

import dal.BillReportDAO;
import dal.RegistrarDAO;
import dal.ScheduleDAO;
import dal.SectionDAO;
import models.billing.Bursar;
import play.mvc.Controller;
import play.mvc.Result;
import viewmodels.ViewCourse;
import viewmodels.ViewProfessor;
import viewmodels.ViewRegistrar;
import viewmodels.ViewSection;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/14/13
 * Time: 11:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class RegistrationController extends Controller {

    public static Result index(int ssn){

        RegistrarDAO regDao = new RegistrarDAO();
        ViewRegistrar registrar = new ViewRegistrar(regDao.constructRegistrar(50));
        return ok(views.html.studentview.registration.render(registrar, registrar.getAllSections().size(), ssn, 50));
    }

    public static Result register(int section, int ssn){

        ScheduleDAO schedDAO = new ScheduleDAO();
        boolean result = schedDAO.studentAddSection(section, ssn);
        if(result){
            SectionDAO sectionDao = new SectionDAO();
            int courseId = sectionDao.getSection(section).getCourseId();
            Bursar bursar = new Bursar();
            bursar.GenerateEBill(ssn, courseId);
            return ok();
        }
        else {
            return badRequest();
        }
    }

    public static Result nextPage(int ssn, int offset){
        RegistrarDAO regDao = new RegistrarDAO();
        ViewRegistrar registrar = new ViewRegistrar(regDao.constructRegistrar(offset));
        return ok(views.html.studentview.registration.render(registrar, registrar.getAllSections().size(), ssn, offset));
    }
}
