package controllers.studentcontrollers;

import dal.BillReportDAO;
import dal.ScheduleDAO;
import dal.SectionDAO;
import models.registration.StudentRegistration;
import play.mvc.Controller;
import play.mvc.Result;
import viewmodels.ViewCourse;
import viewmodels.ViewProfessor;
import viewmodels.ViewSection;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/13/13
 * Time: 10:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleController extends Controller {

    public static Result index(int ssn){
        ScheduleDAO schedDao = new ScheduleDAO();
        StudentRegistration studReg = new StudentRegistration();
        ArrayList<ViewSection> sections = schedDao.getSectionsByStudentId(ssn);
        ArrayList<ViewCourse> courses = studReg.findCoursesBySections(sections);
        ArrayList<ViewProfessor> profs = studReg.findProfessorsBySections(sections);
        int sizeCheck = sections.size();
        // Checking that they are all the same size since they are mapped to one another
        if(courses.size() != sizeCheck || profs.size() != sizeCheck){
            return internalServerError("Error in retrieving schedule from server");
        }
        return ok(views.html.studentview.schedule.render(sections, courses, profs, ssn));
    }

    public static Result drop(int section, int ssn){

        ScheduleDAO schedDao = new ScheduleDAO();
        schedDao.studentDropSection(section, ssn);
        SectionDAO sectionDAO = new SectionDAO();
        int courseId = sectionDAO.getSection(section).getCourseId();
        BillReportDAO billReportDAO = new BillReportDAO();
        billReportDAO.dropBillReport(ssn, courseId);
        return ok();
    }
}
