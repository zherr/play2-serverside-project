package controllers.professorcontrollers;

import controllers.Secured;
import dal.UserDAO;
import models.registration.ProfessorRegistration;
import models.users.User;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import viewmodels.ViewProfessor;
import views.html.professorview.professor;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/7/13
 * Time: 11:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProfessorController extends Controller {

    @Security.Authenticated(Secured.class)
    public static Result index(int ssn){
        //TODO again, this code is smelly. Technically we shouldn't have to check this.
        if(!isProfessor(ssn)){
            return redirect(controllers.studentcontrollers.routes.StudentController.index(ssn));
        }
        ProfessorRegistration reg = new ProfessorRegistration();
        ViewProfessor vProfessor = new ViewProfessor(reg.findProfessorById(ssn));
        return ok(professor.render(vProfessor));
    }

    private static boolean isProfessor(int ssn){
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUser(Integer.toString(ssn));
        // it's a studentview
        if(user.getSalary() == 0){
            return false;
        }
        // it's a professorview
        else{
            return true;
        }
    }
}
