package controllers.professorcontrollers;

import dal.ScheduleDAO;
import dal.SectionDAO;
import dal.StudentDAO;
import dal.TranscriptDAO;
import models.registration.Grade;
import models.registration.ProfessorRegistration;
import models.registration.StudentRegistration;
import models.registration.Transcript;
import play.mvc.Controller;
import play.mvc.Result;
import viewmodels.ViewCourse;
import viewmodels.ViewSection;
import viewmodels.ViewStudent;
import viewmodels.ViewTranscript;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/24/13
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class MySectionsController extends Controller {

    public static Result index(int ssn){

        // Reuse StudentRegistration to get courses by sections
        StudentRegistration studentRegistration = new StudentRegistration();
        SectionDAO sectionDAO = new SectionDAO();
        ProfessorRegistration professorRegistration = new ProfessorRegistration();
        ArrayList<ViewSection> sections = sectionDAO.getSectionsByProfessor(ssn);
        ArrayList<ViewCourse> courses = studentRegistration.findCoursesBySections(sections);
//        ArrayList<ArrayList<ViewStudent>> sectionsStudents = professorRegistration.getStudentBySections(sections);
        return ok(views.html.professorview.mysections.render(sections, courses, ssn));//, sectionsStudents));
    }

    public static Result roster(int ssn, int sectionId){
        TranscriptDAO transcriptDAO = new TranscriptDAO();
        ScheduleDAO scheduleDAO = new ScheduleDAO();
        SectionDAO sectionDAO = new SectionDAO();
        ArrayList<ViewStudent> students = scheduleDAO.getStudentsBySectionId(sectionId);
        ArrayList<Character> grades = new ArrayList<Character>();
        for(ViewStudent stud : students){
            char grade = transcriptDAO.getGradeByStudentAndSection(stud, new ViewSection(sectionDAO.getSection(sectionId)));
            grades.add(grade);
        }
        return ok(views.html.professorview.roster.render(sectionId, ssn, students, grades));
    }

    public static Result editGrade(int ssn, int sectionId, String grade, int studId){
        System.out.println("SectionId: " + sectionId);
        System.out.println("Grade: " + grade);
        StudentRegistration studentRegistration = new StudentRegistration();
        ViewStudent viewStudent = new ViewStudent(studentRegistration.findStudentById(studId));
        SectionDAO sectionDAO = new SectionDAO();
        ViewSection viewSection = new ViewSection(sectionDAO.getSection(sectionId));
        TranscriptDAO transcriptDAO = new TranscriptDAO();
        if(transcriptDAO.getGradeByStudentAndSection(viewStudent, viewSection) == 0){
            transcriptDAO.addTranscript(new Transcript(null, 0, new Grade(0, grade.charAt(0)), studId, sectionId));
        }
        else{
            boolean result = transcriptDAO.updateGrade(studId, sectionId, grade.charAt(0));
            if(result){
                return ok();
            }
            else{
                return badRequest();
            }
        }
        return ok();
    }
}
