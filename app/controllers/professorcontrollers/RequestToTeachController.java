package controllers.professorcontrollers;

import dal.CourseDAO;
import dal.SectionDAO;
import models.course.Section;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import viewmodels.ViewCourse;
import views.html.helper.form;

import java.util.ArrayList;

import static play.data.Form.form;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/24/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class RequestToTeachController extends Controller {

    public static class SectionForm{
        public int parentCourseId;
        @Constraints.Required
        public String sectionNumber;
        public String semester;
        public String yearOffered;
        public int maxSeats;

        public String validate(){
            // TODO make sure the form hits this
            return null;
        }
    }

    public static Result index(int ssn){

        CourseDAO courseDAO = new CourseDAO();
        ArrayList<ViewCourse> courses = courseDAO.getAllCourses(50);
        return ok(views.html.professorview.requesttoteach.render(ssn, courses, form(SectionForm.class), 50));
    }

    public static Result nextPage(int ssn, int offset){
        CourseDAO courseDAO = new CourseDAO();
        ArrayList<ViewCourse> courses = courseDAO.getAllCourses(offset);
        return ok(views.html.professorview.requesttoteach.render(ssn, courses, form(SectionForm.class), offset));
    }

    public static Result professorCreateSection(int ssn){//String sectionNumber, String semester, int year, int instructorSsn, int parentCourse, int maxSeats){

        CourseDAO courseDAO = new CourseDAO();
        ArrayList<ViewCourse> courses = courseDAO.getAllCourses(50);
        Form<SectionForm> sectionForm = form(SectionForm.class).bindFromRequest();
        if(sectionForm.hasErrors()){
            // TODO shouldn't be hitting this before validation...
            System.err.println("Form has errors");
            System.err.println(sectionForm.errors());
            return badRequest(views.html.professorview.requesttoteach.render(ssn, courses, sectionForm, 50));
        }
        else{
            SectionDAO sectionDAO = new SectionDAO();
            Section section = new Section(sectionForm.get().parentCourseId, ssn, sectionForm.get().sectionNumber, sectionForm.get().semester, sectionForm.get().yearOffered, sectionForm.get().maxSeats, 0);
            boolean result = sectionDAO.addSection(section);
            if(result){
                System.err.println("Successful form submit");
                return ok(views.html.professorview.requesttoteach.render(ssn, courses, sectionForm, 50));
            }
            else{
                System.err.println("UNSUCCESSFUL form submit");
                return badRequest(views.html.professorview.requesttoteach.render(ssn, courses, sectionForm, 50));
            }
        }
    }
}
