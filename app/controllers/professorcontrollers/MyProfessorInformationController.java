package controllers.professorcontrollers;

import dal.ProfessorDAO;
import dal.RegistrarDAO;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import viewmodels.ViewProfessor;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/23/13
 * Time: 7:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyProfessorInformationController extends Controller {

    public static Result index(int ssn){
        ProfessorDAO profDao = new ProfessorDAO();
        ViewProfessor professor = new ViewProfessor(profDao.getProfessor(ssn));
        return ok(views.html.professorview.myprofessorinformation.render(professor, ssn));
    }

    public static Result editField(int ssn, String field, String value){
        //Just in case the user gets sneaky...
        if(field.toLowerCase().equals("ssn") || field.toLowerCase().equals("name") ||
                field.toLowerCase().equals("salary")){
            return badRequest();
        }
        System.out.println("Field: " + field);
        System.out.println("Value: " + value);
        RegistrarDAO registrarDAO = new RegistrarDAO();
        boolean result = registrarDAO.updateUserInformation(ssn, field, value);
        if(result){
            return ok();
        }
        else{
            return badRequest();
        }
    }
}
