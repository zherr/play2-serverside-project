package models.registration;

public class Grade {

	private double gradeNumber;
	private char gradeLetter;
	
	public Grade(){ }

    public Grade(double gradeNumber, char gradeLetter) {
        this.gradeNumber = gradeNumber;
        this.gradeLetter = gradeLetter;
    }

    public void setGradeNumber( double gradeNumber ){
		this.gradeNumber = gradeNumber;
	}
	
	public double getGradeNumber(){
		return this.gradeNumber;
	}
	
	public void setGradeLetter( char gradeLetter ){
		this.gradeLetter = gradeLetter;
	}
	
	public char getGradeLetter(){
		return this.gradeLetter;
	}
}
