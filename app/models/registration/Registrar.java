package models.registration;

import models.course.Course;
import models.course.Section;
import models.users.Professor;

import java.util.ArrayList;
import java.util.List;

//TODO this will probably be needed by a professorview if they are given ability to give out grades (transcripts)
public class Registrar {
	
	private List<Professor> allProfessors = new ArrayList<Professor>();
    private List<Course> allCourses = new ArrayList<Course>();
    private List<Section> allSections = new ArrayList<Section>();
    private List<Section> availableSections = new ArrayList<Section>();

    public Registrar(){ }

    public List<Section> getAllSections() {
        return allSections;
    }

    public void setAllSections(List<Section> allSections) {
        this.allSections = allSections;
    }

    public List<Course> getAllCourses() {
        return allCourses;
    }

    public void setAllCourses(List<Course> allCourses) {
        this.allCourses = allCourses;
    }

    public Registrar(List<Professor> allProfessors, List<Course> allCourses, List<Section> allSections) {
        this.allProfessors = allProfessors;
        this.allCourses = allCourses;
        this.allSections = allSections;
        this.availableSections = availableSections;
    }

	public void setAllProfessors( List<Professor> allProfessors ){
		this.allProfessors = allProfessors;
	}
	
	public List<Professor> getAllProfessors(){
		return this.allProfessors;
	}

    public List<Section> getAvailableSections() {
        return availableSections;
    }

    public void setAvailableSections(List<Section> availableSections) {
        this.availableSections = availableSections;
    }
	
    //TODO CreateTranscript
}
