package models.registration;

import java.util.HashMap;

public class Transcript {

	private HashMap<String, Grade> gradeMap = new HashMap<String, Grade>(); //TODO remove this if we don't plan to use it
    private int transcriptId;
    private Grade grade;
    private int studentId;
    private int sectionId;
	
	public Transcript(){ }

    public Transcript(HashMap<String, Grade> gradeMap, int transcriptId, Grade grade, int studentId, int sectionId) {
        this.gradeMap = gradeMap;
        this.transcriptId = transcriptId;
        this.grade = grade;
        this.studentId = studentId;
        this.sectionId = sectionId;
    }

    public int getTranscriptId() {
        return this.transcriptId;
    }

    public void setTranscriptId(int transcriptId) {
        this.transcriptId = transcriptId;
    }

    public Grade getGrade() {
        return this.grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public int getStudentId() {
        return this.studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getSectionId() {
        return this.sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public void setGradeMap( HashMap<String, Grade> gradeMap ){
		this.gradeMap = gradeMap;
	}
	
	public HashMap<String, Grade> getGradeMap(){
		return this.gradeMap;
	}
	
	public Grade getGradeForCourseName( String courseName ){
		return this.gradeMap.get(courseName);
	}
}
