package models.registration;

import dal.CourseDAO;
import dal.ProfessorDAO;
import dal.StudentDAO;
import models.course.Course;
import models.course.Section;
import models.users.Professor;
import models.users.Student;
import viewmodels.ViewCourse;
import viewmodels.ViewProfessor;
import viewmodels.ViewSection;

import java.util.ArrayList;

/**
 * This class acts as a service to student registering for courses or viewing their schedule
 */
public class StudentRegistration {

    public StudentRegistration() { }

    public void RegisterToEnroll( Student studentToEnroll, Section section){
        section.getStudentRoster().add(studentToEnroll);
    }

    public ArrayList<ViewProfessor> findProfessorsBySections(ArrayList<ViewSection> secs){
        ProfessorDAO profDao = new ProfessorDAO();
        ArrayList<ViewProfessor> professors = new ArrayList<ViewProfessor>();
        for(ViewSection sec : secs){
            try{
                Professor prof = profDao.getProfessor(sec.getProfessorId());
                professors.add(new ViewProfessor(prof));
            } catch (Exception e){
                System.err.println("StudentRegistration: Threw exception retrieving professors by section");
                e.printStackTrace();
            }
        }
        return professors;
    }

    public ArrayList<ViewCourse> findCoursesBySections(ArrayList<ViewSection> secs){
        CourseDAO cDao = new CourseDAO();
        ArrayList<ViewCourse> courses = new ArrayList<ViewCourse>();
        for(ViewSection sec : secs){
            try{
                Course course = cDao.getCourse(sec.getCourseId());
                courses.add(new ViewCourse(course));
            } catch (Exception e){
                System.err.println("StudentRegistration: Threw exception retrieving course by section");
                e.printStackTrace();
            }
        }
        return courses;
    }

    public Student findStudentById(int studentSsn) {

        StudentDAO studDAO = new StudentDAO();
        try {
            Student student = studDAO.getStudent(studentSsn);
            return student;
        } catch (Exception se) {
            System.err.println("StudentRegistration: Threw a Exception retrieving studentview.");
            System.err.println(se.getMessage());
        }
        return null;
    }

    public void addStudent(Student student){

        StudentDAO studDAO = new StudentDAO();
        try{
            studDAO.addStudent(student);
        }
        catch(Exception e){
            System.err.println("StudentRegistration: Threw a Exception retrieving customer.");
            System.err.println(e.getMessage());
        }
    }
}
