package models.registration;

import dal.ProfessorDAO;
import dal.ScheduleDAO;
import models.course.Section;
import models.users.Professor;
import viewmodels.ViewSection;
import viewmodels.ViewStudent;

import java.util.ArrayList;

public class ProfessorRegistration {

    public ProfessorRegistration(){ }

    public void RegisterProfessor( Professor profToRegister, Section courseToRegister ){
        //TODO remove this and change to handle real database registration
//        courseToRegister.setInstructor(profToRegister);
    }

    public Professor findProfessorById( int professorSsn ){

        ProfessorDAO profDAO = new ProfessorDAO();
        try {
            Professor professor = profDAO.getProfessor(professorSsn);
            return professor;
        } catch (Exception se) {
            System.err.println("ProfessorRegistration: Threw a Exception retrieving professorview.");
            System.err.println(se.getMessage());
        }
        return null;
    }

    public void addProfessor( Professor prof ){

        ProfessorDAO profDAO = new ProfessorDAO();
        try{
            profDAO.addProfessor(prof);
        }
        catch(Exception e){
            System.err.println("ProfessorRegistration: Threw a Exception retrieving professorview.");
            System.err.println(e.getMessage());
        }
    }

    public ArrayList<ArrayList<ViewStudent>> getStudentBySections(ArrayList<ViewSection> sections){
        ScheduleDAO scheduleDAO = new ScheduleDAO();
        ArrayList<ArrayList<ViewStudent>> sectionsStudents= new ArrayList<ArrayList<ViewStudent>>();
        for(ViewSection section : sections){
            sectionsStudents.add(scheduleDAO.getStudentsBySectionId(section.getSectionId()));
        }
        return sectionsStudents;
    }
}
