package models.users;

public class Professor extends Person{
	
	private String title;
    private int salary;

    public Professor(){ }

    public Professor(String title, int salary) {
        this.title = title;
        this.salary = salary;
    }

    public void setTitle( String title ){
		this.title = title;
	}
	
	public String getTitle(){
		return this.title;
	}
    public int getSalary() {
        return salary;
    }

    public void setSalary( int salary ) {
        this.salary = salary;
    }
}
