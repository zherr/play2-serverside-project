package models.users;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/9/13
 * Time: 3:58 PM
 * To change this template use File | Settings | File Templates.
 */
//TODO this is smelly code. We will probably need to boil down why we need User. Right now it's for the sake of the view
/**
 * Light class that can be used in view.
 */
public class User extends Person{

    private String classStatus;
    private String major;
    private String title;
    private int salary;

    public User() {
    }

    public void setClassStatus( String classStatus ){
        this.classStatus = classStatus;
    }

    public String getClassStatus(){
        return this.classStatus;
    }

    public void setMajor( String major ){
        this.major = major;
    }

    public String getMajor(){
        return this.major;
    }

    public void setTitle( String title ){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }
    public int getSalary() {
        return salary;
    }

    public void setSalary( int salary ) {
        this.salary = salary;
    }
}
