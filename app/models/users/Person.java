package models.users;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/4/13
 * Time: 5:41 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Person {

    private int ssn;
    private String name;
    private String phone;
    private String email;
    private String address;
    private String city;
    private String state;
    private int zip;

    public int getSsn() {
        return this.ssn;
    }

    public void setSsn(int ssn) {
        this.ssn = ssn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setPhone( String phone ){
        this.phone = phone;
    }

    public String getPhone(){
        return this.phone;
    }

    public void setEmail( String email ){
        this.email = email;
    }

    public String getEmail(){
        return this.email;
    }

    public void setAddress( String address ){
        this.address = address;
    }

    public String getAddress(){
        return this.address;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public int getZip() {
        return this.zip;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return this.city;
    }
}
