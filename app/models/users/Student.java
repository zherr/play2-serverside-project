package models.users;

public class Student extends Person{
	
	private String classStatus;
	private String major;

	public Student(){ }

    public Student(String classStatus, String major) {
        this.classStatus = classStatus;
        this.major = major;
    }

    public void setClassStatus( String classStatus ){
        this.classStatus = classStatus;
	}
	
	public String getClassStatus(){
		return this.classStatus;
	}
	
	public void setMajor( String major ){
		this.major = major;
	}
	
	public String getMajor(){
		return this.major;
	}
}
