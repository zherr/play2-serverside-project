package models.billing;

import models.utility.TimeStamp;

public class Payment {
	
	private Cash payment;
	private TimeStamp timeStamp;
	
	public Payment(){ }

    public Payment(Cash payment, TimeStamp timeStamp) {
        this.payment = payment;
        this.timeStamp = timeStamp;
    }

    public void setPayment( Cash payment ){
		this.payment = payment;
	}
	
	public Cash getPayment(){
		return this.payment;
	}
	
	public void setTimeStamp( TimeStamp timeStamp ){
		this.timeStamp = timeStamp;
	}
	
	public TimeStamp getTimeStamp(){
		return this.timeStamp;
	}

}
