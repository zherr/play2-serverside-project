package models.billing;

import dal.BillReportDAO;
import dal.CourseDAO;

import java.util.Calendar;
import java.util.Date;

public class Bursar {

    public Bursar(){ }

    public void GenerateEBill( int studentSsn, int courseId ) {
        CourseDAO courseDAO = new CourseDAO();
        BillReportDAO billReportDAO = new BillReportDAO();

        BillReport bReport = new BillReport();
        Cash courseCost = courseDAO.getCourse(courseId).getCostOfClass();
        Date date = Calendar.getInstance().getTime();
        bReport.setTotalBalance(courseCost);
        bReport.setPaid(false);
        bReport.setCourseId(courseId);
        bReport.setStudentId(studentSsn);
        billReportDAO.addBillReport(bReport);
    }

    public boolean PayBill( int billReportId ){
        BillReportDAO billReportDAO = new BillReportDAO();
        return billReportDAO.payBill(billReportId);
    }
}
