package models.billing;

import models.utility.TimeStamp;

public class BillReport{

    private int billReportId;
	private Cash totalBalance;
    private TimeStamp timeStamp;
    private int studentId;
    private int courseId;
    private boolean paid;

    public BillReport(){ }

    public BillReport(int billReportId, Cash totalBalance, TimeStamp timeStamp, int studentId, int courseId, boolean paid) {
        this.billReportId = billReportId;
        this.totalBalance = totalBalance;
        this.timeStamp = timeStamp;
        this.studentId = studentId;
        this.courseId = courseId;
        this.paid = paid;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getStudentId() {
        return this.studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getBillReportId() {
        return this.billReportId;
    }

    public void setBillReportId(int billReportId) {
        this.billReportId = billReportId;
    }

    public void setTotalBalance( Cash totalBalance ){
		this.totalBalance = totalBalance;
	}
	
	public Cash getTotalBalance(){
		return this.totalBalance;
	}

    public TimeStamp getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(TimeStamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void adjustBalance( Cash adjustBy ){
        totalBalance.addAmount(adjustBy.getAmount());
    }
}
