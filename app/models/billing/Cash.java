package models.billing;

public class Cash {

	private double amount;
	
	public Cash(){ }

    public Cash(double amount) {
        this.amount = amount;
    }

    public void setAmount( double amount ){
		this.amount = amount;
	}
	
	public double getAmount(){
		return this.amount;
	}

    public void addAmount( double amount ){
        this.amount += amount;
    }
    
    public void subtractAmount( double amount ){
    	this.amount -= amount;
    }
}
