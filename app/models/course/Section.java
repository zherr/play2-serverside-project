package models.course;

import models.billing.Cash;
import models.users.Professor;
import models.users.Student;

import java.util.ArrayList;
import java.util.List;

public class Section extends Course{

    private int sectionId;
    private int parentCourseId;
    private int professorId;
	private String sectionNumber;
	private String semester;
    private String yearOffered;
    private List<Student> studentRoster = new ArrayList<Student>();
    protected String location;
    protected int maxSeats;
    protected int currentSeats;
	
	public Section() {}

    public Section(int sectionId, int parentCourseId, int professorId, String sectionNumber, String semester, String yearOffered, int maxSeats, int currentSeats) {
        this.sectionId = sectionId;
        this.parentCourseId = parentCourseId;
        this.professorId = professorId;
        this.sectionNumber = sectionNumber;
        this.semester = semester;
        this.yearOffered = yearOffered;
        this.maxSeats = maxSeats;
        this.currentSeats = currentSeats;
    }

    // Constructor without sectionId
    public Section(int parentCourseId, int professorId, String sectionNumber, String semester, String yearOffered, int maxSeats, int currentSeats) {
        this.parentCourseId = parentCourseId;
        this.professorId = professorId;
        this.sectionNumber = sectionNumber;
        this.semester = semester;
        this.yearOffered = yearOffered;
        this.maxSeats = maxSeats;
        this.currentSeats = currentSeats;
    }

    /**
     * Super constructor for Section
     * @param courseId
     * @param prerequisiteId
     * @param courseName
     * @param description
     * @param prerequisites
     * @param costOfClass
     * @param sectionId
     * @param parentCourseId
     * @param professorId
     * @param sectionNumber
     * @param semester
     * @param yearOffered
     * @param instructor
     * @param studentRoster
     */
    public Section(int courseId, int prerequisiteId, String courseName, String description, List<Course> prerequisites, Cash costOfClass, int sectionId, int parentCourseId, int professorId, String sectionNumber, String semester, String yearOffered, List<Student> studentRoster) {
        super(courseId, prerequisiteId, courseName, description, prerequisites, costOfClass);
        this.sectionId = sectionId;
        this.parentCourseId = parentCourseId;
        this.professorId = professorId;
        this.sectionNumber = sectionNumber;
        this.semester = semester;
        this.yearOffered = yearOffered;
        this.studentRoster = studentRoster;
    }

    public int getProfessorId() {
        return this.professorId;
    }

    public void setProfessorId(int professorId) {
        this.professorId = professorId;
    }

    public int getCourseId() {
        return this.parentCourseId;
    }

    public void setCourseId(int parentCourseId) {
        this.parentCourseId = parentCourseId;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public void setSectionNumber( String sectionNumber ){
		this.sectionNumber = sectionNumber;
	}
	
	public String getSectionNumber(){
		return this.sectionNumber;
	}
	
	public void setSemester( String semester ){
		this.semester = semester;
	}
	
	public String getSemester(){
		return this.semester;
	}

    public String getYearOffered() {
        return this.yearOffered;
    }

    public void setYearOffered(String yearOffered) {
        this.yearOffered = yearOffered;
    }

    public void setStudents( List<Student> students ){
		this.studentRoster = students;
	}
	
	public List<Student> getStudentRoster(){
		return this.studentRoster;
	}
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public int getCurrentSeats() {
        return currentSeats;
    }

    public void setCurrentSeats(int currentSeats) {
        this.currentSeats = currentSeats;
    }
}
