package models.course;

import models.billing.Cash;

import java.util.List;
import java.util.ArrayList;

public class Course {

    protected int courseId;
    protected int prerequisiteId;
	protected String courseName;
	protected String description;
	private List<Course> prerequisites = new ArrayList<Course>();
    protected Cash costOfClass;
    protected int classNumber;
    protected String subj;

    public Course() {}

    public Course(int courseId, int prerequisiteId, String courseName, String description, List<Course> prerequisites, Cash costOfClass) {
        this.courseId = courseId;
        this.prerequisiteId = prerequisiteId;
        this.courseName = courseName;
        this.description = description;
        this.prerequisites = prerequisites;
        this.costOfClass = costOfClass;
    }

    public int getPrerequisiteId() {
        return this.prerequisiteId;
    }

    public void setPrerequisiteId(int prerequisiteId) {
        this.prerequisiteId = prerequisiteId;
    }

    public int getCourseId() {
        return this.courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setCourseName( String courseName ){
		this.courseName = courseName;
	}
	
	public String getCourseName(){
		return this.courseName;
	}
	
	public void setDescription( String type ){
		this.description = type;
	}
	
	public String getDescription(){
		return this.description;
	}

	public void setCoursePrerequisites( List<Course> prerequisites ){
		this.prerequisites = prerequisites;
	}
	
	public List<Course> getPrerequisites(){
		return this.prerequisites;
	}

    public void setCostOfClass( Cash costOfClass ){
        this.costOfClass = costOfClass;
    }

    public Cash getCostOfClass(){
        return this.costOfClass;
    }

    public int getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(int classNumber) {
        this.classNumber = classNumber;
    }

    public String getSubj() {
        return subj;
    }

    public void setSubj(String subj) {
        this.subj = subj;
    }
}
