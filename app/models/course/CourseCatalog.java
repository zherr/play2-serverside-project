package models.course;

import models.utility.TimeStamp;

import java.util.ArrayList;
import java.util.List;

public class CourseCatalog {

	private TimeStamp dateUpdated;
	private List<Section> allSections = new ArrayList<Section>();
    private List<Course> allCourses = new ArrayList<Course>();
	
	public CourseCatalog(){ }

    public CourseCatalog(TimeStamp dateUpdated, List<Section> allSections, List<Course> allCourses) {
        this.dateUpdated = dateUpdated;
        this.allSections = allSections;
        this.allCourses = allCourses;
    }

    public void setDateUpdated( TimeStamp date ){
		this.dateUpdated = date;
	}
	
	public TimeStamp getDateUpdated(){
		return this.dateUpdated;
	}
	
	public void setAllSections(List<Section> allSections){
		this.allSections = allSections;
	}
	
	public List<Section> getAllSections(){
		return this.allSections;
	}

    public List<Course> getAllCourses() {
        return this.allCourses;
    }

    public void setAllCourses(List<Course> allCourses) {
        this.allCourses = allCourses;
    }
}
