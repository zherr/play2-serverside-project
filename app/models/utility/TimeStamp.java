package models.utility;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 9/30/13
 * Time: 9:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class TimeStamp {

    private int month;
    private int day;
    private int year;
    private Time time;

    public TimeStamp() {}

    public TimeStamp(int month, int day, int year, Time time) {
        this.month = month;
        this.day = day;
        this.year = year;
        this.time = time;
    }

    /**
     * Constructor
     * @param stdTimeString - a standard string in the form of YYYY-MM-DD 00:00:00
     */
    public TimeStamp( String stdTimeString ){
        String[] split = stdTimeString.split("[\\s+\\:\\-\\.]");
        this.year = Integer.parseInt(split[0]);
        this.month = Integer.parseInt(split[1]);
        this.day = Integer.parseInt(split[2]);
        Time t = new Time();
        t.setHours(Integer.parseInt(split[3]));
        t.setMinutes(Integer.parseInt(split[4]));
        t.setSeconds(Integer.parseInt(split[5]));
        t.setmSecs(0);
        this.time = t;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        String finalString = "";

        finalString += this.year;
        finalString += "-";
        finalString += (this.month < 10) ? ("0" + this.month) : this.month;
        finalString += "-";
        finalString += (this.day < 10) ? ("0" + this.day) : this.day;
        finalString += " ";
        finalString += this.time.toString();
        return finalString;
    }
}
