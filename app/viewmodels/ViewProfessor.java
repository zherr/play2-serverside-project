package viewmodels;

import models.users.Professor;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/7/13
 * Time: 9:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewProfessor {

    private Professor vProfessor;

    public ViewProfessor(Professor professor){
        this.vProfessor = professor == null ? null : professor;
    }

    public String getTitle(){
        return this.vProfessor.getTitle();
    }
    public int getSalary() {
        return this.vProfessor.getSalary();
    }

    public int getSsn() {
        return this.vProfessor.getSsn();
    }

    public String getName() {
        return this.vProfessor.getName();
    }

    public String getPhone(){
        return this.vProfessor.getPhone();
    }

    public String getEmail(){
        return this.vProfessor.getEmail();
    }

    public String getAddress(){
        return this.vProfessor.getAddress();
    }

    public int getZip() {
        return this.vProfessor.getZip();
    }

    public String getState() {
        return this.vProfessor.getState();
    }

    public String getCity() {
        return this.vProfessor.getCity();
    }
}
