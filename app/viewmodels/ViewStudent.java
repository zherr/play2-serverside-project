package viewmodels;

import models.users.Student;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/7/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewStudent {

    private Student vStudent;

    public ViewStudent(Student student){
        this.vStudent = student == null ? null : student;
    }

    public String getClassStatus(){
        return this.vStudent.getClassStatus();
    }

    public String getMajor(){
        return this.vStudent.getMajor();
    }

    public int getSsn() {
        return this.vStudent.getSsn();
    }

    public String getName() {
        return this.vStudent.getName();
    }

    public String getPhone(){
        return this.vStudent.getPhone();
    }

    public String getEmail(){
        return this.vStudent.getEmail();
    }

    public String getAddress(){
        return this.vStudent.getAddress();
    }

    public int getZip() {
        return this.vStudent.getZip();
    }

    public String getState() {
        return this.vStudent.getState();
    }

    public String getCity() {
        return this.vStudent.getCity();
    }
}
