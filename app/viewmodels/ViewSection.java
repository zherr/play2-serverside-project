package viewmodels;

import models.course.Section;
import models.users.Student;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/12/13
 * Time: 11:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewSection {

    private Section section;

    public ViewSection(Section section){
        this.section = section;
    }

    public int getProfessorId() {
        return this.section.getProfessorId();
    }

    public int getCourseId() {
        return this.section.getCourseId();
    }

    public int getSectionId() {
        return this.section.getSectionId();
    }

    public String getSectionNumber(){
        return this.section.getSectionNumber();
    }

    public String getSemester(){
        return this.section.getSemester();
    }

    public String getYearOffered() {
        return this.section.getYearOffered();
    }

    public List<Student> getStudentRoster(){
        return this.section.getStudentRoster();
    }

    public int getMaxSeats(){
        return this.section.getMaxSeats();
    }

    public int getCurrentSeats(){
        return this.section.getCurrentSeats();
    }

}
