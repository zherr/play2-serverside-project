package viewmodels;

import models.course.Course;
import models.course.Section;
import models.registration.Registrar;
import models.users.Professor;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/19/13
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewRegistrar {
    private Registrar registrar;

    public ViewRegistrar(Registrar registrar){
        this.registrar = registrar;
    }

    public ArrayList<ViewProfessor> getAllProfessors(){
        ArrayList<ViewProfessor> profs = new ArrayList<ViewProfessor>();
        for(Professor prof : this.registrar.getAllProfessors()){
            profs.add(new ViewProfessor(prof));
        }
        return profs;
    }

    public ArrayList<ViewCourse> getAllCourses(){
        ArrayList<ViewCourse> courses = new ArrayList<ViewCourse>();
        for(Course course : this.registrar.getAllCourses()){
            courses.add(new ViewCourse(course));
        }
        return courses;
    }

    public ArrayList<ViewCourse> getAvailableCourses(){
        ArrayList<ViewCourse> courses = new ArrayList<ViewCourse>();
        for(Course course : this.registrar.getAvailableSections()){
            courses.add(new ViewCourse(course));
        }
        return courses;
    }

    public ArrayList<ViewSection> getAllSections(){
        ArrayList<ViewSection> sections = new ArrayList<ViewSection>();
        for(Section section : this.registrar.getAllSections()){
            sections.add(new ViewSection(section));
        }
        return sections;
    }
}
