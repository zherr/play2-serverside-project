package viewmodels;

import models.billing.Cash;
import models.course.Course;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/13/13
 * Time: 11:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewCourse {

    private Course course;

    public ViewCourse(Course course){
        this.course = course;
    }

    public int getPrerequisiteId() {
        return this.course.getPrerequisiteId();
    }

    public int getCourseId() {
        return this.course.getCourseId();
    }

    public String getCourseName(){
        return this.course.getCourseName();
    }

    public String getDescription(){
        return this.course.getDescription();
    }

    public List<Course> getPrerequisites(){
        return this.course.getPrerequisites();
    }

    public Cash getCostOfClass(){
        return this.course.getCostOfClass();
    }
}
