package viewmodels;

import models.billing.BillReport;
import models.billing.Cash;
import models.utility.TimeStamp;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/17/13
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewBillReport {

    private BillReport billReport;

    public ViewBillReport( BillReport billReport ){
        this.billReport = billReport;
    }

    public int getStudentId() {
        return this.billReport.getStudentId();
    }

    public int getBillReportId() {
        return this.billReport.getBillReportId();
    }

    public Cash getTotalBalance(){
        return this.billReport.getTotalBalance();
    }

    public TimeStamp getTimeStamp() {
        return this.billReport.getTimeStamp();
    }

    public int getCourseId() {
        return this.billReport.getCourseId();
    }

    public boolean isPaid(){
        return this.billReport.isPaid();
    }
}
