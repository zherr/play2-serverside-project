package viewmodels;

import models.registration.Grade;
import models.registration.Transcript;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/10/13
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewTranscript {

    // Additional information held by this class for the view
    private String sectionNumber;
    private String courseName;
    private String profName;

    private Transcript transcript;

    public ViewTranscript(String sectionNumber, String courseName, String profName, Transcript transcript) {
        this.sectionNumber = sectionNumber;
        this.courseName = courseName;
        this.profName = profName;
        this.transcript = transcript;
    }

    public int getTranscriptId() {
        return this.transcript.getTranscriptId();
    }

    public char getGradeLetter() {
        return this.transcript.getGrade().getGradeLetter();
    }

    public int getStudentId() {
        return this.transcript.getStudentId();
    }

    public int getSectionId() {
        return this.transcript.getSectionId();
    }

    public HashMap<String, Grade> getGradeMap(){
        return this.transcript.getGradeMap();
    }

    public Grade getGradeForCourseName( String courseName ){
        return this.transcript.getGradeMap().get(courseName);
    }

    public String getSectionNumber() {
        return this.sectionNumber;
    }

    public String getCourseName() {
        return this.courseName;
    }

    public String getProfName() {
        return this.profName;
    }

}
