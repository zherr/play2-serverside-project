package dal;

import models.billing.BillReport;
import models.billing.Cash;
import models.utility.TimeStamp;
import viewmodels.ViewBillReport;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/9/13
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class BillReportDAO {

    public BillReportDAO() { }

    public BillReport getBillReport( int billReportId ){

        try{
            // Get BillReport
            Statement st = DBHelper.getConnection().createStatement();

            String selectBillReportQuery = "SELECT billReportId, balance, dateBilled, studentSsn, courseId, paid FROM Bill_Reports WHERE billReportId = '" + billReportId + "'";

            ResultSet billReportRS = st.executeQuery(selectBillReportQuery);
            System.out.println("BillReportDAO: *************** Query " + selectBillReportQuery);

            //Get BillReport
            BillReport billReport = new BillReport();
            while ( billReportRS.next() ) {
                billReport.setBillReportId(billReportRS.getInt("billReportId"));
                Cash tuition = new Cash();
                Cash total = new Cash();
                total.setAmount(billReportRS.getDouble("balance"));
                billReport.setTotalBalance(total);
                TimeStamp timeStamp = new TimeStamp(billReportRS.getString("dateBilled"));
                assert timeStamp != null;
                billReport.setTimeStamp(timeStamp);
                billReport.setStudentId(billReportRS.getInt("studentSsn"));
                billReport.setCourseId(billReportRS.getInt("courseId"));
                billReport.setPaid(billReportRS.getBoolean("paid"));
            }
            //close to manage resources
            billReportRS.close();
            DBHelper.closeConnection();

            return billReport;
        }
        catch( SQLException se ){
            System.err.println("BillReportDAO: Threw a SQLException retrieving the BillReport object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void addBillReport( BillReport billReport ){

        Connection con = DBHelper.getConnection();
        PreparedStatement billReportPst = null;
        PreparedStatement addPst = null;

        try{
            //Insert the BillReport object
            String billStmt = "INSERT INTO Bill_Reports(billReportId, balance, studentSsn, courseId, paid) VALUES(?, ?, ?, ?, ?)";
            billReportPst = con.prepareStatement(billStmt);
            billReportPst.setInt(1, billReport.getBillReportId());
            billReportPst.setDouble(2, billReport.getTotalBalance().getAmount());
            billReportPst.setInt(3, billReport.getStudentId());
            billReportPst.setInt(4, billReport.getCourseId());
            billReportPst.setBoolean(5, billReport.isPaid());
            billReportPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("BillReportDAO: Threw SQLException while adding BillReport to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    billReportPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("BillReportDAO: Threw a SQLException saving the BillReport object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    public ArrayList<ViewBillReport> getBillReportsByStudentSsn(int ssn){
        ArrayList<ViewBillReport> billReports = new ArrayList<ViewBillReport>();
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectBillReportQuery = "SELECT * FROM `bill_reports` WHERE `bill_reports`.`studentSsn` = " + ssn;

            ResultSet billReportRS = st.executeQuery(selectBillReportQuery);
            System.out.println("BillReportDAO: *************** Query " + selectBillReportQuery);

            while ( billReportRS.next() ) {
                BillReport billReport = new BillReport();
                billReport.setBillReportId(billReportRS.getInt("billReportId"));
                Cash tuition = new Cash();
                Cash total = new Cash();
                total.setAmount(billReportRS.getDouble("balance"));
                billReport.setTotalBalance(total);
                TimeStamp timeStamp = new TimeStamp(billReportRS.getString("dateBilled"));
                billReport.setTimeStamp(timeStamp);
                billReport.setStudentId(billReportRS.getInt("studentSsn"));
                billReport.setCourseId(billReportRS.getInt("courseId"));
                billReport.setPaid(billReportRS.getBoolean("paid"));
                billReports.add(new ViewBillReport(billReport));
            }
            //close to manage resources
            billReportRS.close();
            DBHelper.closeConnection();

            return billReports;
        }
        catch( SQLException se ){
            System.err.println("BillReportDAO: Threw a SQLException retrieving the BillReport object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void dropBillReport(int studentSsn, int courseId){
        try{
            Statement st = DBHelper.getConnection().createStatement();
            // Only delete if it has not been paid
            String deleteSql = "DELETE FROM `bill_reports` WHERE `studentSsn`= " + studentSsn + " AND `courseId` = " +
                    courseId + " AND `paid` != 1";
            int result = st.executeUpdate(deleteSql);
        } catch( SQLException e ){
            System.err.println("BillReportDAO: Threw exception when trying to drop a bill report");
            e.printStackTrace();
        }
    }

    public boolean payBill(int billReportId){
        try{
            Statement st = DBHelper.getConnection().createStatement();
            String updatePaid = "UPDATE `bill_reports` SET `paid` = 1 WHERE `bill_reports`.`billReportId` = " + billReportId;
            System.out.println("Paying bill: " + updatePaid);
            st.executeUpdate(updatePaid);
            return true;
        } catch( SQLException e ){
            System.err.println("BillReportDAO: Threw exception when trying to pay a bill report");
            System.err.println(e.getMessage());
            return false;
        }
    }
}
