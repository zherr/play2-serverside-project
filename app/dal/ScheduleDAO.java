package dal;

import models.course.Section;
import models.users.Student;
import viewmodels.ViewSection;
import viewmodels.ViewStudent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/12/13
 * Time: 11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleDAO {

    public ScheduleDAO(){ }

    /**
     * Gets the student within a certain section
     * @param sectionId - the id of the section
     * @return ArrayList of Students in the section
     */
    public ArrayList<ViewStudent> getStudentsBySectionId(int sectionId){

        ArrayList<ViewStudent> studsInSec = new ArrayList<ViewStudent>();
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectStudentBySectionSql = "SELECT `ssn`, `name`, `phone`, `email`, `address`, `city`, `state`, `zip`, `major`, `classStatus` " +
                    "FROM `people`, `people_sections` " +
                    "WHERE `people`.`ssn` = `people_sections`.`studentSsn` AND `people_sections`.`sectionId` = " + sectionId;

            ResultSet studSecRS = st.executeQuery(selectStudentBySectionSql);
            System.out.println("ScheduleDao: *************** Query " + selectStudentBySectionSql);

            while ( studSecRS.next() ) {
                Student student = new Student();
                student.setSsn(studSecRS.getInt("ssn"));
                student.setName(studSecRS.getString("name"));
                student.setPhone(studSecRS.getString("phone"));
                student.setEmail(studSecRS.getString("email"));
                student.setAddress(studSecRS.getString("address"));
                student.setCity(studSecRS.getString("city"));
                student.setState(studSecRS.getString("state"));
                student.setZip(studSecRS.getInt("zip"));
                student.setMajor(studSecRS.getString("major"));
                student.setClassStatus(studSecRS.getString("classStatus"));
                studsInSec.add(new ViewStudent(student));
            }
            //close to manage resources
            studSecRS.close();
            DBHelper.closeConnection();

            return studsInSec;
        }
        catch( SQLException se ){
            System.err.println("ScheduleDAO: Threw a SQLException retrieving the students in a section .");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    /**
     * Gets the sections taken by a particular student
     * @param ssn - The ssn of the student
     * @return ArrayList of sections taken by the student
     */
    public ArrayList<ViewSection> getSectionsByStudentId(int ssn){

        ArrayList<ViewSection> sections = new ArrayList<ViewSection>();
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectSectionsByStudentSql = "SELECT `sections`.`sectionId`, `sectionNumber`, `semesterOffered`, `yearOffered`, `instructor`, `parentCourse` " +
                    "FROM `people_sections`, `sections` " +
                    "WHERE `sections`.`sectionId` = `people_sections`.`sectionId` AND `people_sections`.`studentSsn` = " + ssn;

            ResultSet sectStudRS = st.executeQuery(selectSectionsByStudentSql);
            System.out.println("ScheduleDAO: *************** Query " + selectSectionsByStudentSql);

            while ( sectStudRS.next() ) {
                Section sec = new Section();
                sec.setSectionId(sectStudRS.getInt("sectionId"));
                sec.setSectionNumber(sectStudRS.getString("sectionNumber"));
                sec.setSemester(sectStudRS.getString("semesterOffered"));
                sec.setYearOffered(sectStudRS.getString("yearOffered"));
                sec.setProfessorId(sectStudRS.getInt("instructor"));
                sec.setCourseId(sectStudRS.getInt("parentCourse"));
                sections.add(new ViewSection(sec));
            }
            //close to manage resources
            sectStudRS.close();
            DBHelper.closeConnection();

            return sections;
        }
        catch( SQLException se ){
            System.err.println("ScheduleDAO: Threw a SQLException retrieving the sections by student .");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void studentDropSection(int sectionId, int ssn){
        String deleteSql = "DELETE FROM `people_sections` WHERE `sectionId`= " + sectionId + " AND `studentSsn` = " + ssn;
        try{
            Statement st = DBHelper.getConnection().createStatement();
            int result = st.executeUpdate(deleteSql);
        } catch( SQLException e ){
            System.err.println("ScheduleDAO: Threw exception when trying to have a student drop a section");
            System.err.println(deleteSql);
            e.printStackTrace();
        }
    }

    public boolean studentAddSection(int sectionId, int ssn){
        try{
            Statement st =  DBHelper.getConnection().createStatement();
            String addSql = "INSERT INTO `people_sections` VALUES(" + ssn + ", " + sectionId + ")";
            st.execute(addSql);
            return true;
        } catch(SQLException e){
            System.err.println("ScheduleDAO: Threw exception when trying to add a student to a section");
            e.printStackTrace();
            return false;
        }
    }
}
