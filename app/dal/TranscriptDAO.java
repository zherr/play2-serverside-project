package dal;

import models.registration.Grade;
import models.registration.Transcript;
import viewmodels.ViewSection;
import viewmodels.ViewStudent;
import viewmodels.ViewTranscript;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/9/13
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class TranscriptDAO {
    public TranscriptDAO() { }

    public Transcript getTranscript( int transcriptId ){

        try{
            // Get Transcript
            Statement st = DBHelper.getConnection().createStatement();

            String selectTranscriptQuery = "SELECT transcriptId, gradeNumber, gradeLetter, studentSsn, sectionId FROM Transcripts WHERE transcriptId = '" + transcriptId + "'";

            ResultSet transcriptRS = st.executeQuery(selectTranscriptQuery);
            System.out.println("TranscriptDAO: *************** Query " + selectTranscriptQuery);

            //Get Transcript
            Transcript transcript = new Transcript();
            while ( transcriptRS.next() ) {
                transcript.setTranscriptId(transcriptRS.getInt("transcriptId"));
                Grade g = new Grade();
                g.setGradeNumber(transcriptRS.getDouble("gradeNumber"));
                g.setGradeLetter(transcriptRS.getString("gradeLetter").charAt(0));
                transcript.setGrade(g);
                transcript.setStudentId(transcriptRS.getInt("studentSsn"));
                transcript.setSectionId(transcriptRS.getInt("sectionId"));
            }
            //close to manage resources
            transcriptRS.close();
            DBHelper.closeConnection();

            return transcript;
        }
        catch( SQLException se ){
            System.err.println("TranscriptDAO: Threw a SQLException retrieving the transcript object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void addTranscript( Transcript transcript ){

        Connection con = DBHelper.getConnection();
        PreparedStatement transcriptPst = null;
        PreparedStatement addPst = null;

        try{
            //Insert the transcript object
            String sectionStmt = "INSERT INTO transcripts(gradeNumber, gradeLetter, studentSsn, sectionId) VALUES(?, ?, ?, ?)";
            transcriptPst = con.prepareStatement(sectionStmt);
            transcriptPst.setDouble(1, transcript.getGrade().getGradeNumber());
            transcriptPst.setString(2, String.valueOf(transcript.getGrade().getGradeLetter()));
            transcriptPst.setInt(3, transcript.getStudentId());
            transcriptPst.setInt(4, transcript.getSectionId());
            transcriptPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("TranscriptDAO: Threw SQLException while adding transcript to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    transcriptPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("TranscriptDAO: Threw a SQLException saving the transcript object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    public ArrayList<ViewTranscript> getTranscriptsStringByStudent(ViewStudent student){
        ArrayList<ViewTranscript> transcripts = new ArrayList<ViewTranscript>();

        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectTranscriptQuery = "SELECT transcriptId, gradeNumber, gradeLetter, studentSsn, `transcripts`.`sectionId`, sectionNumber, courseName, name FROM `transcripts`, `sections`, `courses`, `people` " +
                    "WHERE studentSsn = " + student.getSsn() + " AND `transcripts`.`sectionId` = `sections`.`sectionId` AND `sections`.`instructor` = `people`.`ssn` " +
                    "AND `sections`.`parentCourse` = `courses`.`courseId`";

            ResultSet transcriptRS = st.executeQuery(selectTranscriptQuery);
            System.out.println("TranscriptDAO: *************** Query " + selectTranscriptQuery);

            while ( transcriptRS.next() ) {
                Transcript transcript = new Transcript();
                transcript.setTranscriptId(transcriptRS.getInt("transcriptId"));
                Grade g = new Grade();
                g.setGradeNumber(transcriptRS.getDouble("gradeNumber"));
                g.setGradeLetter(transcriptRS.getString("gradeLetter").charAt(0));
                transcript.setGrade(g);
                transcript.setStudentId(transcriptRS.getInt("studentSsn"));
                transcript.setSectionId(transcriptRS.getInt("sectionId"));
                transcripts.add(new ViewTranscript(transcriptRS.getString("sectionNumber"), transcriptRS.getString("courseName"), transcriptRS.getString("name"), transcript ));
            }
            //close to manage resources
            transcriptRS.close();
            DBHelper.closeConnection();

            return transcripts;
        }
        catch( SQLException se ){
            System.err.println("TranscriptDAO: Threw a SQLException retrieving the transcript object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;
    }

    public char getGradeByStudentAndSection(ViewStudent student, ViewSection section){
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectTranscriptQuery = "SELECT * FROM `transcripts` " +
                    "WHERE studentSsn = " + student.getSsn() + " AND sectionId = " + section.getSectionId();

            ResultSet transcriptRS = st.executeQuery(selectTranscriptQuery);
            System.out.println("TranscriptDAO: *************** Query " + selectTranscriptQuery);

            char grade = 0;
            while ( transcriptRS.next() ) {
                grade = transcriptRS.getString("gradeLetter").charAt(0);
            }
            //close to manage resources
            transcriptRS.close();
            DBHelper.closeConnection();
            System.out.println("Grade:" + grade);
            return grade;
        }
        catch( SQLException se ){
            System.err.println("TranscriptDAO: Threw a SQLException retrieving the transcript object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return 0;
    }

    public boolean updateGrade(int ssn, int sectionId, char grade){
        try{
            Statement st = DBHelper.getConnection().createStatement();
            String updatePaid = "UPDATE `transcripts` SET `gradeLetter` = '" + grade + "' WHERE `transcripts`.`studentSsn` = " + ssn + " AND `sectionId` = " + sectionId;
            st.executeUpdate(updatePaid);
            return true;
        } catch( SQLException e ){
            System.err.println("TranscriptDAO: Threw exception when trying to update a grade");
            System.err.println(e.getMessage());
            return false;
        }
    }
}
