package dal;

import models.course.Section;
import viewmodels.ViewSection;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/9/13
 * Time: 10:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class SectionDAO {

    public SectionDAO() { }

    public Section getSection( int sectionId ){

        try{
            // Get Section
            Statement st = DBHelper.getConnection().createStatement();

            String selectSectionQuery = "SELECT sectionId, sectionNumber, semesterOffered, yearOffered, instructor, parentCourse, maxSeats, currentSeats FROM Sections WHERE sectionId = '" + sectionId + "'";

            ResultSet sectionRS = st.executeQuery(selectSectionQuery);
            System.out.println("SectionDAO: *************** Query " + selectSectionQuery);

            //Get Section
            Section section = new Section();
            while ( sectionRS.next() ) {
                section.setSectionId(sectionRS.getInt("sectionId"));
                section.setSectionNumber(sectionRS.getString("sectionNumber"));
                section.setSemester(sectionRS.getString("semesterOffered"));
                section.setYearOffered(sectionRS.getString("yearOffered"));
                section.setProfessorId(sectionRS.getInt("instructor"));
                section.setCourseId(sectionRS.getInt("parentCourse"));
                section.setMaxSeats(sectionRS.getInt("maxSeats"));
                section.setCurrentSeats(sectionRS.getInt("currentSeats"));
            }
            //close to manage resources
            sectionRS.close();
            DBHelper.closeConnection();

            return section;
        }
        catch( SQLException se ){
            System.err.println("SectionDAO: Threw a SQLException retrieving the section object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public boolean addSection( Section section ){

        Connection con = DBHelper.getConnection();
        PreparedStatement sectionPst = null;
        PreparedStatement addPst = null;
        boolean success = true;

        try{
            //Insert the section object
            String sectionStmt = "INSERT INTO sections(sectionId, sectionNumber, semesterOffered, yearOffered, instructor, parentCourse, maxSeats, currentSeats) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
            sectionPst = con.prepareStatement(sectionStmt);
            sectionPst.setString(1, null);
            sectionPst.setString(2, section.getSectionNumber());
            sectionPst.setString(3, section.getSemester());
            sectionPst.setString(4, section.getYearOffered());
            sectionPst.setInt(5, section.getProfessorId());
            sectionPst.setInt(6, section.getCourseId());
            sectionPst.setInt(7, section.getMaxSeats());
            sectionPst.setInt(8, section.getCurrentSeats());
            sectionPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("SectionDAO: Threw SQLException while adding section to database.");
            System.err.println(se.getMessage());
            success = false;
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    sectionPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("SectionDAO: Threw a SQLException saving the section object.");
                System.err.println(ex.getMessage());
                return success;
            }
            return true;
        }
    }

    public ArrayList<ViewSection> getSectionsByProfessor(int ssn){
        ArrayList<ViewSection> sections = new ArrayList<ViewSection>();
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectSectionsByProfessor = "SELECT * FROM `sections` WHERE `sections`.`instructor` = " + ssn;

            ResultSet sectProfRS = st.executeQuery(selectSectionsByProfessor);
            System.out.println("SectionDAO: *************** Query " + selectSectionsByProfessor);

            while ( sectProfRS.next() ) {
                Section sec = new Section();
                sec.setSectionId(sectProfRS.getInt("sectionId"));
                sec.setSectionNumber(sectProfRS.getString("sectionNumber"));
                sec.setSemester(sectProfRS.getString("semesterOffered"));
                sec.setYearOffered(sectProfRS.getString("yearOffered"));
                sec.setProfessorId(sectProfRS.getInt("instructor"));
                sec.setCourseId(sectProfRS.getInt("parentCourse"));
                sec.setMaxSeats(sectProfRS.getInt("maxSeats"));
                sec.setCurrentSeats(sectProfRS.getInt("currentSeats"));
                sections.add(new ViewSection(sec));
            }
            //close to manage resources
            sectProfRS.close();
            DBHelper.closeConnection();

            return sections;
        }
        catch( SQLException se ){
            System.err.println("SectionDAO: Threw a SQLException retrieving the sections by professor .");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }
}
