package dal;

import models.billing.Cash;
import models.course.Course;
import models.course.Section;
import models.registration.Registrar;
import models.users.Professor;
import viewmodels.ViewCourse;
import viewmodels.ViewProfessor;
import viewmodels.ViewSection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/16/13
 * Time: 3:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class RegistrarDAO {

    public RegistrarDAO(){ }

    public Registrar constructRegistrar(int offset){
        ArrayList<Section> sections = new ArrayList<Section>();
        ArrayList<Course> courses = new ArrayList<Course>();
        ArrayList<Professor> professors = new ArrayList<Professor>();
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectAllSections = "SELECT * FROM `sections` INNER JOIN `people` ON " +
                    "`sections`.`instructor` = `people`.`ssn` INNER JOIN `courses` ON `sections`.`parentCourse` = " +
                    "`courses`.`courseId` ORDER BY `courses`.`courseName` ASC LIMIT 50 OFFSET " + offset;
//                    "SELECT * FROM `sections`, `people`, `courses` WHERE `sections`.`parentCourse` = "
//                        + "`courses`.`courseId` AND `sections`.`instructor` = `people`.`ssn` LIMIT 0,10";
            // TODO limiting to 10 for now. Ideally search instead of list.

            ResultSet sectionsRS = st.executeQuery(selectAllSections);
            System.out.println("RegistrarDAO: *************** Query " + selectAllSections);

            while ( sectionsRS.next() ) {
                // Section info
                Section section = new Section();
                section.setStudents(null);
                section.setSectionId(sectionsRS.getInt("sectionId"));
                section.setSectionNumber(sectionsRS.getString("sectionNumber"));
                section.setSemester(sectionsRS.getString("semesterOffered"));
                section.setYearOffered(sectionsRS.getString("yearOffered"));
                section.setProfessorId(sectionsRS.getInt("instructor"));
                section.setCourseId(sectionsRS.getInt("parentCourse"));
                sections.add(section);
                // Course info
                Course course = new Course();
                course.setCourseId(sectionsRS.getInt("courseId"));
                course.setPrerequisiteId(sectionsRS.getInt("prerequisiteId"));
                course.setCourseName(sectionsRS.getString("courseName"));
                course.setDescription(sectionsRS.getString("courseDescr"));
                Cash cash = new Cash();
                cash.setAmount(sectionsRS.getDouble("cost"));
                course.setCostOfClass(cash);
                courses.add(course);
                // Professor info
                Professor professor = new Professor();
                professor.setSsn(sectionsRS.getInt("ssn"));
                professor.setName(sectionsRS.getString("name"));
                professor.setPhone(sectionsRS.getString("phone"));
                professor.setEmail(sectionsRS.getString("email"));
                professor.setAddress(sectionsRS.getString("address"));
                professor.setCity(sectionsRS.getString("city"));
                professor.setState(sectionsRS.getString("state"));
                professor.setZip(sectionsRS.getInt("zip"));
                professor.setTitle(sectionsRS.getString("title"));
                professor.setSalary(sectionsRS.getInt("salary"));
                professors.add(professor);
            }
            Registrar registrar = new Registrar(professors, courses, sections);
            //close to manage resources
            sectionsRS.close();
            DBHelper.closeConnection();

            return registrar;
        }
        catch( SQLException se ){
            System.err.println("RegistrarDAO: Threw a SQLException retrieving the all sections .");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public ArrayList<ViewSection> getAllSections(){
        ArrayList<ViewSection> sections = new ArrayList<ViewSection>();
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectAllSections = "SELECT `sectionId`, `sectionNumber`, `semesterOffered`, `yearOffered`, " +
                    "`instructor`, `parentCourse`, `courseName` FROM `sections` INNER JOIN `people` ON " +
                    "`sections`.`instructor` = `people`.`ssn` INNER JOIN `courses` ON `sections`.`parentCourse` = " +
                    "`courses`.`courseId` LIMIT 0,10";
            // TODO limiting to 10 for now. Ideally search instead of list.

            ResultSet sectionsRS = st.executeQuery(selectAllSections);
            System.out.println("RegistrarDAO: *************** Query " + selectAllSections);

            while ( sectionsRS.next() ) {
                Section section = new Section();
                section.setStudents(null);
                section.setSectionId(sectionsRS.getInt("sectionId"));
                section.setSectionNumber(sectionsRS.getString("sectionNumber"));
                section.setSemester(sectionsRS.getString("semesterOffered"));
                section.setYearOffered(sectionsRS.getString("yearOffered"));
                section.setProfessorId(sectionsRS.getInt("instructor"));
                section.setCourseId(sectionsRS.getInt("parentCourse"));
                sections.add(new ViewSection(section));
            }
            //close to manage resources
            sectionsRS.close();
            DBHelper.closeConnection();

            return sections;
        }
        catch( SQLException se ){
            System.err.println("RegistrarDAO: Threw a SQLException retrieving the all sections .");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public ArrayList<ViewSection> getAvailableSections(){
        ArrayList<ViewSection> sections = new ArrayList<ViewSection>();
        try{
            Statement st = DBHelper.getConnection().createStatement();
//
//            String selectAvailableSections = "SELECT `sectionId`, `sectionNumber`, `semesterOffered`, `yearOffered`, " +
//                    "`instructor`, `parentCourse`, `courseName` FROM `sections` WHERE `sections`.`semesterOffered` = 'Spring' " +
//                    " INNER JOIN `people` ON " +
//                    "`sections`.`instructor` = `people`.`ssn` INNER JOIN `courses` ON `sections`.`parentCourse` = " +
//                    "`courses`.`courseId`";

            String selectAvailableSections = "SELECT `sectionId`, `sectionNumber`, `semesterOffered`, `yearOffered` " +
                    " FROM `sections` WHERE `semesterOffered` = " + "Spring" + "";

            ResultSet sectionsRS = st.executeQuery(selectAvailableSections);
            System.out.println("RegistrarDAO: *************** Query " + getAvailableSections());

            while ( sectionsRS.next() ) {
                Section section = new Section();
                section.setStudents(null);
                section.setSectionId(sectionsRS.getInt("sectionId"));
                section.setSectionNumber(sectionsRS.getString("sectionNumber"));
                section.setSemester(sectionsRS.getString("semesterOffered"));
                section.setYearOffered(sectionsRS.getString("yearOffered"));
//                section.setProfessorId(sectionsRS.getInt("instructor"));
//                section.setCourseId(sectionsRS.getInt("parentCourse"));
                sections.add(new ViewSection(section));
            }
            //close to manage resources
            sectionsRS.close();
            DBHelper.closeConnection();

            return sections;
        }
        catch( SQLException se ){
            System.err.println("RegistrarDAO: Threw a SQLException retrieving the available sections .");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }


    public ArrayList<ViewProfessor> getProfessorsBySections(ArrayList<ViewSection> sections){
        ArrayList<ViewProfessor> profs = new ArrayList<ViewProfessor>();
        // So we can get a professor by id
        ProfessorDAO profDao = new ProfessorDAO();
        for(int i = 0; i < sections.size(); ++i){
            ViewProfessor prof = new ViewProfessor(profDao.getProfessor(sections.get(i).getProfessorId()));
            profs.add(prof);
        }
        return profs;
    }

    /**
     * Gets each sections parent course
     * @param sections - the sections to find parent courses for
     * @return ArrayList of ViewCourses corresponding to thee sections passed in
     */
    public ArrayList<ViewCourse> getCoursesBySections(ArrayList<ViewSection> sections){
        ArrayList<ViewCourse> courses = new ArrayList<ViewCourse>();
        // So we can get a course by its id
        CourseDAO courseDAO = new CourseDAO();
        for(int i = 0; i < sections.size(); ++i){
            ViewCourse viewCourse = new ViewCourse(courseDAO.getCourse(sections.get(i).getCourseId()));
            courses.add(viewCourse);
        }
        return courses;
    }

    public boolean updateUserInformation(int ssn, String fieldName, String value){

        try{
            Statement st = DBHelper.getConnection().createStatement();
            String updateInfo = "UPDATE `people` SET `" + fieldName + "` = '" + value + "' WHERE `people`.`ssn` = " + ssn;
            System.out.println("RegistrarDAO: stmt => " + updateInfo);
            st.executeUpdate(updateInfo);
            return true;
        } catch( SQLException e ){
            System.err.println("RegisterDAO: Threw exception when trying to pay a bill report");
            System.err.println(e.getMessage());
            return false;
        }
    }
}
