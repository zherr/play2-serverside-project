package dal;

import models.users.Professor;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/6/13
 * Time: 2:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProfessorDAO {

    public ProfessorDAO() { }

    public Professor getProfessor( int ssn ){

        try{
            // Get Professor
            Statement st = DBHelper.getConnection().createStatement();

            String selectCustomerQuery = "SELECT ssn, name, phone, email, address, city, state, zip, title, salary FROM People WHERE ssn = '" + ssn + "'";

            ResultSet profRS = st.executeQuery(selectCustomerQuery);
            System.out.println("ProfessorDAO: *************** Query " + selectCustomerQuery);

            //Get Student
            Professor professor = new Professor();
            while ( profRS.next() ) {
                professor.setSsn(profRS.getInt("ssn"));
                professor.setName(profRS.getString("name"));
                professor.setPhone(profRS.getString("phone"));
                professor.setEmail(profRS.getString("email"));
                professor.setAddress(profRS.getString("address"));
                professor.setCity(profRS.getString("city"));
                professor.setState(profRS.getString("state"));
                professor.setZip(profRS.getInt("zip"));
                professor.setTitle(profRS.getString("title"));
                professor.setSalary(profRS.getInt("salary"));
            }
            //close to manage resources
            profRS.close();
            DBHelper.closeConnection();

            return professor;
        }
        catch( SQLException se ){
            System.err.println("ProfessorDAO: Threw a SQLException retrieving the professorview object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void addProfessor( Professor prof ){

        Connection con = DBHelper.getConnection();
        PreparedStatement profPst = null;
        PreparedStatement addPst = null;

        try{
            //Insert the professorview object
            String profStmt = "INSERT INTO People(ssn, name, phone, email, address, city, state, zip, title, salary) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            profPst= con.prepareStatement(profStmt);
            profPst.setInt(1, prof.getSsn());
            profPst.setString(2, prof.getName());
            profPst.setString(3, prof.getPhone());
            profPst.setString(4, prof.getEmail());
            profPst.setString(5, prof.getAddress());
            profPst.setString(6, prof.getCity());
            profPst.setString(7, prof.getState());
            profPst.setInt(8, prof.getZip());
            profPst.setString(9, prof.getTitle());
            profPst.setInt(10, prof.getSalary());
            profPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("ProfessorDAO: Threw SQLException while adding professorview to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    profPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("ProfessorDAO: Threw a SQLException saving the professorview object.");
                System.err.println(ex.getMessage());
            }
        }
    }
}
