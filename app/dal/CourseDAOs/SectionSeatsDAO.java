package dal.CourseDAOs;

import dal.DBHelper;
import models.course.Section;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: Joe
 * Date: 11/16/13
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectionSeatsDAO {

    public SectionSeatsDAO(){ }

    public Section getSectionSeats( int courseId ){

        try{
            // Get Course
            Statement st = DBHelper.getConnection().createStatement();

            String selectCourseQuery= "SELECT CLASS_NUMBER, ENRL_CAP FROM course_seats WHERE CLASS_NUMBER = '" + courseId + "'";

            ResultSet courseRS = st.executeQuery(selectCourseQuery);
            System.out.println("SectionSeatsDAO: *************** Query " + selectCourseQuery);

            //Get Course
            Section section = new Section();
            while ( courseRS.next() ) {
                section.setClassNumber(courseRS.getInt("CLASS_NUMBER"));
                section.setMaxSeats(courseRS.getInt("ENRL_CAP"));
            }
            //close to manage resources
            courseRS.close();
            DBHelper.closeConnection();

            return section;
        }
        catch( SQLException se ){
            System.err.println("SectionSeatsDAO: Threw a SQLException retrieving the course object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void addCourseSeats( Section section ){

        Connection con = DBHelper.getConnection();
        PreparedStatement coursePst = null;
        PreparedStatement addPst = null;

        try{
            //Insert the section object
            String courseStmt = "INSERT INTO course_seats(SELECT CLASS_NUMBER, ENRL_CAP) VALUES(?, ?)";
            coursePst = con.prepareStatement(courseStmt);
            coursePst.setInt(1, section.getClassNumber());
            coursePst.setInt(2, section.getMaxSeats());
            coursePst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("SectionSeatsDAO: Threw SQLException while adding course to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    coursePst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("SectionSeatsDAO: Threw a SQLException saving the course object.");
                System.err.println(ex.getMessage());
            }
        }
    }
}
