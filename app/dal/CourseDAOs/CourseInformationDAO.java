package dal.CourseDAOs;

import dal.DBHelper;
import models.billing.Cash;
import models.course.Course;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Joe
 * Date: 11/16/13
 * Time: 8:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class CourseInformationDAO {

    public CourseInformationDAO() { }

    public Course getCourse( int courseId ){

        try{
            // Get Course
            Statement st = DBHelper.getConnection().createStatement();

            //TODO this is looking for where course = class_number because we have multiple course numbers
            //TODO IE class_number is unique where course number is not (see course information table)
            String selectCourseQuery= "SELECT CLASS_NUMBER, SUBJ_CD, COURSE_NUM, COURSE_TITLE, COURSE_DESC FROM course_information WHERE CLASS_NUMBER = '" + courseId + "'";

            ResultSet courseRS = st.executeQuery(selectCourseQuery);
            System.out.println("CourseInformationDAO: *************** Query " + selectCourseQuery);

            //Get Course
            Course course = new Course();
            while ( courseRS.next() ) {
                course.setClassNumber(courseRS.getInt("CLASS_NUMBER"));
                course.setSubj(courseRS.getString("SUBJ_CD"));
                course.setCourseId(courseRS.getInt("COURSE_NUM"));
                course.setCourseName(courseRS.getString("COURSE_TITLE"));
                course.setDescription(courseRS.getString("courseDescr"));
//                Cash cash = new Cash();
//                cash.setAmount(courseRS.getDouble("cost"));
//                course.setCostOfClass(cash);
            }
            //close to manage resources
            courseRS.close();
            DBHelper.closeConnection();

            return course;
        }
        catch( SQLException se ){
            System.err.println("CourseInformationDAO: Threw a SQLException retrieving the course object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void addCourse( Course course ){

        Connection con = DBHelper.getConnection();
        PreparedStatement coursePst = null;
        PreparedStatement addPst = null;

        try{
            //Insert the section object
            String courseStmt = "INSERT INTO course_information(SELECT CLASS_NUMBER, SUBJ_CD, COURSE_NUM, COURSE_TITLE, COURSE_DESC) VALUES(?, ?, ?, ?, ?)";
            coursePst = con.prepareStatement(courseStmt);
            coursePst.setInt(1, course.getClassNumber());
            coursePst.setString(2, course.getSubj());
            coursePst.setInt(3, course.getCourseId());
            coursePst.setString(4, course.getCourseName());
            coursePst.setString(5, course.getDescription());


//            coursePst.setInt(2, course.getPrerequisiteId());
//            coursePst.setDouble(5, course.getCostOfClass().getAmount());
            coursePst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("CourseInformationDAO: Threw SQLException while adding course to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    coursePst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("CourseInformationDAO: Threw a SQLException saving the course object.");
                System.err.println(ex.getMessage());
            }
        }
    }
   //TODO I didn't touch this yet. Need to make a descision on how to implement pre-recs with new data
    public List<Course> getCoursePrerequisites( int prerequisiteId ){

        ArrayList<Course> prereqs = new ArrayList<Course>();
        try{
            // Get Course prerequisites
            Statement st = DBHelper.getConnection().createStatement();

            String selectPrereqQuery = "SELECT Courses.* FROM Courses JOIN Prerequisites on Courses.courseId = Prerequisites.courseId WHERE Prerequisites.prereqId = '" + prerequisiteId + "'";

            ResultSet prereqsRS = st.executeQuery(selectPrereqQuery);
            System.out.println("CourseInformationDAO: *************** Query " + selectPrereqQuery);

            while ( prereqsRS.next() ) {
                Course course = new Course();
                course.setCourseId(prereqsRS.getInt("courseId"));
                course.setPrerequisiteId(prereqsRS.getInt("prerequisiteId"));
                course.setCourseName(prereqsRS.getString("courseName"));
                course.setDescription(prereqsRS.getString("courseDescr"));
                Cash cash = new Cash();
                cash.setAmount(prereqsRS.getDouble("cost"));
                course.setCostOfClass(cash);
                prereqs.add(course);
            }
            //close to manage resources
            prereqsRS.close();

            return prereqs;
        }
        catch( SQLException se ){
            System.err.println("CourseInformationDAO: Threw a SQLException retrieving the course prerequisites.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return prereqs; //could be empty
    }
}
