package dal;

import models.users.Student;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/4/13
 * Time: 11:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class StudentDAO {

    public StudentDAO() { }

    public Student getStudent( int ssn ){

        try{
            // Get Student
            Statement st = DBHelper.getConnection().createStatement();

            String selectCustomerQuery = "SELECT ssn, name, phone, email, address, city, state, zip, major, classStatus FROM People WHERE ssn = '" + ssn + "'";

            ResultSet studRS = st.executeQuery(selectCustomerQuery);
            System.out.println("StudentDAO: *************** Query " + selectCustomerQuery);

            //Get Student
            Student student = new Student();
            while ( studRS.next() ) {
                student.setSsn(studRS.getInt("ssn"));
                student.setName(studRS.getString("name"));
                student.setPhone(studRS.getString("phone"));
                student.setEmail(studRS.getString("email"));
                student.setAddress(studRS.getString("address"));
                student.setCity(studRS.getString("city"));
                student.setState(studRS.getString("state"));
                student.setZip(studRS.getInt("zip"));
                student.setMajor(studRS.getString("major"));
                student.setClassStatus(studRS.getString("classStatus"));
            }
            //close to manage resources
            studRS.close();
            DBHelper.closeConnection();

            return student;
        }
        catch( SQLException se ){
            System.err.println("StudentDAO: Threw a SQLException retrieving the studentview object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void addStudent(Student student){

        Connection con = DBHelper.getConnection();
        PreparedStatement studPst = null;
        PreparedStatement addPst = null;

        try{
            //Insert the studentview object
            String studStmt = "INSERT INTO People(ssn, name, phone, email, address, city, state, zip, major, classStatus) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            studPst = con.prepareStatement(studStmt);
            studPst.setInt(1, student.getSsn());
            studPst.setString(2, student.getName());
            studPst.setString(3, student.getPhone());
            studPst.setString(4, student.getEmail());
            studPst.setString(5, student.getAddress());
            studPst.setString(6, student.getCity());
            studPst.setString(7, student.getState());
            studPst.setInt(8, student.getZip());
            studPst.setString(9, student.getMajor());
            studPst.setString(10, student.getClassStatus());
            studPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("CustomerDAO: Threw SQLException while adding studentview to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    studPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("StudentDAO: Threw a SQLException saving the studentview object.");
                System.err.println(ex.getMessage());
            }
        }
    }
}
