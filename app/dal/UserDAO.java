package dal;

import models.users.User;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 11/9/13
 * Time: 3:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserDAO {

    public UserDAO() { }

    public User getUser(String ssn){

        try{
            // Get User
            Statement st = DBHelper.getConnection().createStatement();

            String selectCustomerQuery = "SELECT ssn, name, phone, email, address, city, state, zip, major, classStatus, title, salary FROM People WHERE ssn = '" + ssn + "'";

            ResultSet userRS = st.executeQuery(selectCustomerQuery);
            System.out.println("UserDAO: *************** Query " + selectCustomerQuery);

            User user = new User();
            while ( userRS.next() ) {
                user.setSsn(userRS.getInt("ssn"));
                user.setName(userRS.getString("name"));
                user.setPhone(userRS.getString("phone"));
                user.setEmail(userRS.getString("email"));
                user.setAddress(userRS.getString("address"));
                user.setCity(userRS.getString("city"));
                user.setState(userRS.getString("state"));
                user.setZip(userRS.getInt("zip"));
                user.setMajor(userRS.getString("major"));
                user.setClassStatus(userRS.getString("classStatus"));
                user.setTitle(userRS.getString("title"));
                user.setSalary(userRS.getInt("salary"));
            }
            //close to manage resources
            userRS.close();
            DBHelper.closeConnection();

            return user;
        }
        catch( SQLException se ){
            System.err.println("UserDAO: Threw a SQLException retrieving the user object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }
}
