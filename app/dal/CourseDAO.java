package dal;

import models.billing.Cash;
import models.course.Course;
import viewmodels.ViewCourse;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/6/13
 * Time: 4:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class CourseDAO {

    public CourseDAO() { }

    public Course getCourse( int courseId ){

        try{
            // Get Course
            Statement st = DBHelper.getConnection().createStatement();

            String selectCourseQuery= "SELECT courseId, prerequisiteId, courseName, courseDescr, cost FROM Courses WHERE courseId = '" + courseId + "'";

            ResultSet courseRS = st.executeQuery(selectCourseQuery);
            System.out.println("CourseDAO: *************** Query " + selectCourseQuery);

            //Get Course
            Course course = new Course();
            while ( courseRS.next() ) {
                course.setCourseId(courseRS.getInt("courseId"));
                course.setPrerequisiteId(courseRS.getInt("prerequisiteId"));
                course.setCourseName(courseRS.getString("courseName"));
                course.setDescription(courseRS.getString("courseDescr"));
                Cash cash = new Cash();
                cash.setAmount(courseRS.getDouble("cost"));
                course.setCostOfClass(cash);
            }
            //close to manage resources
            courseRS.close();
            DBHelper.closeConnection();

            return course;
        }
        catch( SQLException se ){
            System.err.println("CourseDAO: Threw a SQLException retrieving the course object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    public void addCourse( Course course ){

        Connection con = DBHelper.getConnection();
        PreparedStatement coursePst = null;
        PreparedStatement addPst = null;

        try{
            //Insert the section object
            String courseStmt = "INSERT INTO courses(courseId, prerequisiteId, courseName, courseDescr, cost) VALUES(?, ?, ?, ?, ?)";
            coursePst = con.prepareStatement(courseStmt);
            coursePst.setInt(1, course.getCourseId());
            coursePst.setInt(2, course.getPrerequisiteId());
            coursePst.setString(3, course.getCourseName());
            coursePst.setString(4, course.getDescription());
            coursePst.setDouble(5, course.getCostOfClass().getAmount());
            coursePst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("CourseDAO: Threw SQLException while adding course to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    coursePst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("CourseDAO: Threw a SQLException saving the course object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    public List<Course> getCoursePrerequisites( int prerequisiteId ){

        ArrayList<Course> prereqs = new ArrayList<Course>();
        try{
            // Get Course prerequisites
            Statement st = DBHelper.getConnection().createStatement();

            String selectPrereqQuery = "SELECT Courses.* FROM Courses JOIN Prerequisites on Courses.courseId = Prerequisites.courseId WHERE Prerequisites.prereqId = '" + prerequisiteId + "'";

            ResultSet prereqsRS = st.executeQuery(selectPrereqQuery);
            System.out.println("CourseDAO: *************** Query " + selectPrereqQuery);

            while ( prereqsRS.next() ) {
                Course course = new Course();
                course.setCourseId(prereqsRS.getInt("courseId"));
                course.setPrerequisiteId(prereqsRS.getInt("prerequisiteId"));
                course.setCourseName(prereqsRS.getString("courseName"));
                course.setDescription(prereqsRS.getString("courseDescr"));
                Cash cash = new Cash();
                cash.setAmount(prereqsRS.getDouble("cost"));
                course.setCostOfClass(cash);
                prereqs.add(course);
            }
            //close to manage resources
            prereqsRS.close();

            return prereqs;
        }
        catch( SQLException se ){
            System.err.println("CourseDAO: Threw a SQLException retrieving the course prerequisites.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return prereqs; //could be empty
    }

    public ArrayList<ViewCourse> getAllCourses(int offset){
        ArrayList<ViewCourse> courses = new ArrayList<ViewCourse>();
        try{
            Statement st = DBHelper.getConnection().createStatement();

            String selectAllCoursesQuery = "SELECT * FROM courses ORDER BY  `courses`.`courseName` ASC LIMIT 50 OFFSET " + offset;

            ResultSet coursesRS = st.executeQuery(selectAllCoursesQuery);
            System.out.println("CourseDAO: *************** Query " + selectAllCoursesQuery);

            while ( coursesRS.next() ) {
                Course course = new Course();
                course.setCourseId(coursesRS.getInt("courseId"));
                course.setPrerequisiteId(coursesRS.getInt("prerequisiteId"));
                course.setCourseName(coursesRS.getString("courseName"));
                course.setDescription(coursesRS.getString("courseDescr"));
                Cash cash = new Cash();
                cash.setAmount(coursesRS.getDouble("cost"));
                course.setCostOfClass(cash);
                courses.add(new ViewCourse(course));
            }
            //close to manage resources
            coursesRS.close();
            DBHelper.closeConnection();

            return courses;
        }
        catch( SQLException se ){
            System.err.println("CourseDAO: Threw a SQLException retrieving all courses.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }
}
