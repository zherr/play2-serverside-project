/*
 * jQuery Raptorize Plugin 1.0
 * www.ZURB.com/playground
 * Copyright 2010, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/


(function($) {

    jQuery_1_4_1.fn.raptorize = function(options) {

        //Yo' defaults
        var defaults = {  
            enterOn: 'click', //timer, konami-code, click
            delayTime: 5000 //time before raptor attacks on timer mode
            };  
        
        //Extend those options
        var options = jQuery_1_4_1.extend(defaults, options);
	
        return this.each(function() {

			var _this = jQuery_1_4_1(this);
			var audioSupported = false;
			//Stupid Browser Checking which should be in jQuery Support
			if (jQuery_1_4_1.browser.mozilla && jQuery_1_4_1.browser.version.substr(0, 5) >= "1.9.2" || jQuery_1_4_1.browser.webkit) {
				audioSupported = true;
			}
			
			//Raptor Vars
			var raptorImageMarkup = '<img id="elRaptor" style="display: none" src="/assets/images/raptor.png" />'
			var raptorAudioMarkup = '<audio id="elRaptorShriek" preload="auto"><source src="assets/sound/raptor-sound.mp3" /><source src="/assets/sound/raptor-sound.ogg" /></audio>';
			var locked = false;
			
			//Append Raptor and Style
			jQuery_1_4_1('body').append(raptorImageMarkup);
 			if(audioSupported) { jQuery_1_4_1('body').append(raptorAudioMarkup); }
			var raptor = jQuery_1_4_1('#elRaptor').css({
				"position":"fixed",
				"bottom": "-700px",
				"right" : "0",
				"display" : "block"
			})
			
			// Animating Code
			function init() {
				locked = true;
			
				//Sound Hilarity
				if(audioSupported) { 
					function playSound() {
						document.getElementById('elRaptorShriek').play();
					}
					playSound();
				}
								
				// Movement Hilarity	
				raptor.animate({
					"bottom" : "0"
				}, function() { 			
					jQuery_1_4_1(this).animate({
						"bottom" : "-130px"
					}, 100, function() {
						var offset = ((jQuery_1_4_1(this).position().left)+400);
						$(this).delay(300).animate({
							"right" : offset
						}, 2200, function() {
							raptor = jQuery_1_4_1('#elRaptor').css({
								"bottom": "-700px",
								"right" : "0"
							})
							locked = false;
						})
					});
				});
			}
			
			
			//Determine Entrance
			if(options.enterOn == 'timer') {
				setTimeout(init, options.delayTime);
			} else if(options.enterOn == 'click') {
				_this.bind('click', function(e) {
					e.preventDefault();
					if(!locked) {
						init();
					}
				})
			} else if(options.enterOn == 'konami-code'){
			    var kkeys = [], konami = "38,38,40,40,37,39,37,39,66,65";
			    jQuery_1_4_1(window).bind("keydown.raptorz", function(e){
			        kkeys.push( e.keyCode );
			        if ( kkeys.toString().indexOf( konami ) >= 0 ) {
			        	init();
			        	jQuery_1_4_1(window).unbind('keydown.raptorz');
			        }
			    }, true);
	
			}
			
        });//each call
    }//orbit plugin call
})(jQuery);

