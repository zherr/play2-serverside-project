$(function() {
    var sectionNumber = $( "#sectionNumer" ),
        semester = $( "#semester" ),
        year = $( "#year" ),
        maxSeats = $( "#maxSeats" ),
        allFields = $( [] ).add( sectionNumber ).add( semester ).add( year ).add( maxSeats ),
        tips = $( ".validateTips" );

    function updateTips( t ) {
        tips
            .text( t )
            .addClass( "ui-state-highlight" );
        setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
        }, 500 );
    }

    function checkLength( o, n, min, max ) {
        if ( o.val().length > max || o.val().length < min ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be between " +
                min + " and " + max + "." );
            return false;
        } else {
            return true;
        }
    }

    function checkRange( o, n, max) {
        if ( o.val().toString().length > max ) {
            o.addClass( "ui-state-error" );
            updateTips( "Length of " + n + " must be less than " +
                max + "." );
            return false;
        } else {
            return true;
        }
    }

    function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
            o.addClass( "ui-state-error" );
            updateTips( n );
            return false;
        } else {
            return true;
        }
    }

    $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 600,
        width: 700,
        modal: true,
        buttons: {
            "Create a new section": function() {
                var bValid = true;
                allFields.removeClass( "ui-state-error" );

                //bValid = bValid && checkLength( city, "city", 2, 30 );
                //bValid = bValid && checkRange( zip, "zip", 5 );

                if ( bValid ) {
                    $( "#section-form" ).submit();
                } else {
                    alert("Form not valid");
                }
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        close: function() {
            allFields.val( "" ).removeClass( "ui-state-error" );
        }
    });

    $( "#create-section" )
        .button()
        .click(function() {
            $( "#dialog-form" ).dialog( "open" );
        });
});
